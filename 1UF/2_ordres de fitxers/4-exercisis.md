#
﻿Exercicis de fitxers (04)
	Conceptes clau:
* Enllaç simbòlic i enllaç dur.
* Canviar de nom fitxers
* Comprarar fitxers
* Comprimir/descomprimir, dividir/unir.
Ordres a treballar:
ln, ln -s, ls -li, stat
[ rename ]
diff, cmp, diff3, file
split, cat, zcat, gzip, gunzip, bzip, 7zip
Exercicis
________________

Primera part----------------------------------------------------------------------------------------
1. Fer actiu el directori /var/tmp/m01.

# cd /var/tmp/m01/

2. Crear dins m01 els fitxers informe.txt, dosier.odt, circular.pdf
# touch informe.txt dosier.odt circular.pdf

3. Crear dins el directori m01  un ellaç dur al (hard link) fitxer informe.txt anomenat newinforme.txt.
#  ln informe.txt newinforme.txt
4. Llistar el directori m01 amb ls -li i observar els inodes.
# ls -li
- 524305 -rw-r--r-- 2 trenau trenau 0 Oct 27 15:49 informe.txt
- 524305 -rw-r--r-- 2 trenau trenau 0 Oct 27 15:49 newinforme.txt

5. Crear dins el directori m01 un enllaç dur (hard link) al fitxer informe.txt anomenat renewinforme.txt.
# ln informe.txt renewinforme.txt

6. Llistar el directori m01 amb ls -li i observar els inodes.
# ls -li
- 524305 -rw-r--r-- 3 trenau trenau 0 Oct 27 15:49 informe.txt
- 524305 -rw-r--r-- 3 trenau trenau 0 Oct 27 15:49 newinforme.txt
- 524305 -rw-r--r-- 3 trenau trenau 0 Oct 27 15:49 renewinforme.txt

7. Crear al directori prova (/tmp/prova) un enllaç dur anomenat biinforme.txt que apunti al fitxer informe.txt de m01.

# ln ./informe.txt /tmp/prova/biinforme.txt
 
8. Crear al directori prova (/tmp/prova) un enllaç dur anomenat triinforme.txt que apunti al fitxer informe.txt de m01.
#  ln informe.txt /tmp/prova/triinforme.txt
9. Llistar els noms i inodes del directori prova.
# ls -li /tmp/provatmp/
- 524305 -rw-r--r-- 5 trenau trenau 0 Oct 27 15:49 biinforme.txt
- 524305 -rw-r--r-- 5 trenau trenau 0 Oct 27 15:49 triinforme.txt

10. Aplicar l’ordre stat als fitxers i directoris  d’aquesta primera part d’exercicis
# stat informe.txt 
  File: informe.txt
  Size: 0         	Blocks: 0          IO Block: 4096   regular empty file
Device: 10305h/66309d	Inode: 524305      Links: 5
Access: (0644/-rw-r--r--)  Uid: ( 1000/  trenau)   Gid: ( 1000/  trenau)
Access: 2022-10-27 15:49:17.137361191 +0200
Modify: 2022-10-27 15:49:17.137361191 +0200
Change: 2022-10-27 15:57:42.025750265 +0200
Birth: 2022-10-27 15:49:17.137361191 +0200

Segona part ------------------------------------------------------------------------------------
11. Fer actiu el directori /var/tmp
12. Crear dins del directori prova (/tmp/prova) un enllaç dur a cada un dels fitxers anomenats carta del directori m01 (/var/tmp/m01).  (de cop). 

# ln m01/carta* /tmp/prova/

13. Llistar el contingut i els inodes de prova.
# ls -li /tmp/prova/
total 8
524305 -rw-r--r-- 5 trenau trenau  0 Oct 27 15:49 biinforme.txt
524335 -rw-r--r-- 2 trenau trenau 79 Oct 27 22:17 carta1.txt
524380 -rw-r--r-- 2 trenau trenau 90 Oct 27 22:17 carta2.txt
524305 -rw-r--r-- 5 trenau trenau  0 Oct 27 15:49 triinforme.txt

14.  Crear al directori prova un enlaç dur al directori operatius de m01.
#  NO ES PODEN FER ln ENTRE DIRECTORIS 

15.  Crear al directori prova un enlaç al directori operatius de m01. Quin tipus d’enllaç cal?
#  ln -s /tmp/prova/  m01/  	(-s => symbolic link) 

16.  Crear dins de prova un enllaç simbòlic anomanat newdossier.odt que apunti al fitxer dossier.odt de m01.
#  ln -s m01/dosier.odt /tmp/prova/newdosier.odt
 
17.  Crear dins de prova un enllaç simbòlic anomanat renewdossier.odt que apunti al fitxer dossier.odt de m01.
#  ln -s m01/dosier.odt prova/renewdossier.odt

18.  Crear dins de m01  un enllaç simbòlic anomanat bidossier.odt que apunti al fitxer dossier.odt de m01. Llistar i mostrar el contingut de bidossier.odt.

19.  Crear dins de m01  un enllaç simbòlic anomanat tridossier.odt que apunti al fitxer dossier.odt de m01. Llistar i mostrar el contingut de tridossier.odt.

20.  Crear dins de prova  un enllaç simbòlic anomanat newfstab del fitxer /etc/fstab.

#  ln -s /etc/fstab prova/newfstab

21.  Crear dins de prova un enllaç simbòlic anomanat arrancada del directori /boot.

Tercera part ---------------------------------------------------------------------------------------
[rename: no fer aquest bloc]
22.  Fer actiu el directori /var/tmp
23.  Llistar els fitxers de m01 (/var/tmp/m01) d’extensió txt.
24.  Modificar el mom els fitxers txt de m01 posant ara l’extensió md.
25.  Modificar el nom dels fitxers que comencen per carta de m01, posant en lloc de carta letter.
26.  Modificar el nom dels fitxers de m01 que comencen per letter i tenen extensió md posant com a nou nom envelop (part inicial) i l’extensió rtf.

Quarta part-----------------------------------------------------------------------------------------

27.  Fer actiu el directori /var/tmp/m01
28.  Comparar els fitxers informe.md i newinformew.md
# cmp informe.txt newinforme.txt 
29.  Editar el fitxer informe.txt i comparar-los de nou. Continuen essent iguals?
# cat > informe.txt 
# cmp informe.txt newinforme.txt
# si, continuen essent iguals els dos informes  
30.  Comparar els fitxers dossier.md i newdossier.md
31.  Editar el fitxer dossier.odt i comparar-los de nou. Continuen essent iguals?.
32.  Comprar els fitxers informe.md i dossier.odt.
#  cmp informe.txt newinforme.txt
33.  Copiar el fitxer  informe.txt creant-ne un de nou anomenat informe.txt. Editar informe.txt i afegir una línia inicial, modificar una paraula del mig del text i afegir una línia final.
# cp informe.txt informe.txt.
# cat > informe.txt 
	linea inicial
# cat > informe.txt.
	linea final
# cmp informe.txt informe.txt.
	informe.txt informe.txt. differ: byte 7, line 1
	
34. Comparar informe.md i informe.txt.
35. Comparar informe.md i newinforme.md.

Cinquena part -----------------------------------------------------------------------------------

36. Crear dins de /var/tmp el directori parts.
37. Fer actiu el directori /vat/tmp/parts.
38. Amb l’ordre whereis o which identificar l’executable de l’ordre ls.
# whereis ls
	ls: /usr/bin/ls /usr/share/man/man1/ls.1.gz
	
# which ls
	/usr/bin/ls
	
39. Copiar l’executable de l’ordre ls al directori parts.
# cp /usr/bin/ls .
40. Comparar el fitxer copiat del ls amb l’original.
#  cmp /usr/bin/ls ls 
41. Dividir el fitxer ls del directori parts en tres fragments diferents. Llistar els fragments generats. Examinar amb file de quin tipus són.
#  split -n 3 ls
#  ls -lh
#  file ls
	ls: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, 		interpreter /lib64/ld-linux-x86-64.so.2,	   BuildID[sha1]=6461a544c35b9dc1d172d1a1c09043e487326966, for GNU/Linux 3.2.0, stripped

42. Ajuntar amb cat els tres fragments generant un nou fitxer anomenat newls (al directori parts).
# cat xaa xab xac > newls

43. Compara el fitxers newls, ls del directori parts i el ls original.
# cmp ls newls 
44. Executar l’ordre /var/tmp/parts/newls
45. Dividir el fitxer ls de parts en  fragments de 50K anomenant cada fragment amb el prefix llistar.
# ls -lh
	total 436K
	-rw-r--r-- 1 trenau trenau  50K Oct 30 15:04 llistar.aa
	-rw-r--r-- 1 trenau trenau  50K Oct 30 15:04 llistar.ab
	-rw-r--r-- 1 trenau trenau  44K Oct 30 15:04 llistar.ac
	-rwxr-xr-x 1 trenau trenau 144K Oct 30 13:30 ls

46. Ajuntar de nou aquests fragments anteriors usant cat i generant el fitxer llistarls.
# cat llistar.aa llistar.ab llistar.ac > llistarls 
47. Compara llistarls, nels, ls i l’original de ls.
# cmp llistarls newls 
# cmp llistarls ls
# ( no es poden comparar mes de dos fitxers a la vegada) 

# diff3 llistarls newls ls

48. Executar amb la ruta absoluta llistarls. Executar ./llistarls.
49. Eliminar totes les parts fetes en aquest apartat.

Sisena part ------------------------------------------------------------------------------------

50. Fer actiu el directori /var/tmp/parts, on hi haurà una còpia de l’executable de l’ordre ls.
51. Copiar l’executable de l’ordre cal i de l’ordre date al directori parts.
# whereis cal 
	cal: /usr/bin/cal /usr/share/man/man1/cal.1.gz
# cmp /usr/bin/cal 

# which date 
	/usr/bin/date
# cp /usr/bin/date

52. Comprimir el fitxer date de parts usant gzip. Que li passa al original?
# gzip date 
	(Al original no li afecta ja que desde un principi ja esat comprimit)
53. Descomprimir date.gz amb gunzip.
# gunzip date.gz 
54. Comprimir el fitxer cal amb bzip a màxima compressió. Llistar-los. Descomprimir-lo.
# bzip2 cal 
# ls -lhi 
# bzip2 -d cal 
55. Instal·lar 7zip i fer una prova de compressió i descompressió d’un fitxer.
 
Pràctica final ------------------------------------------------------------------------------------
56. Des del directori actiu parts (/var/tmp/parts) dividir crear un gzip de l’executable de l’ordre ls. Dividir aquest ls.gz en tres parts. Generar la unió de les parts fabricant el fitxer newls.gz i  a continuació descomprimir-lo. Ffinalment comprovar que ls i newls són el mateix contingut i executar la comanda newls.

# gzip ls 
# split -n3 ls.gz 
# cat x* > newls.gz
# gunzip newls.gz
# cmp ls.gz newls 
	(ls.gz newls differ: byte 1, line 1)
# gunzip ls.gz
# cmp ls newls 





