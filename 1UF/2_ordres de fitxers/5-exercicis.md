# 
﻿Exercicis de fitxers (05)
	Conceptes clau:
* Informació d’usuari
* Quotes i ocupació de disc.
* Buscar fitxers (nom, mida, propietari, tipus)
* Empaquetar contingut: tar
Ordres a treballar:
id, who, finger, chfn, chsh, .plan
Quota, du, du --max-depth, du -s, df
updatedb, locate
find, find --size, find --user, find --type
tar, tar -pP
Exercicis
________________
Primera part ------------------------------------------------------------------------------------
1.  Fer actiu el directori /var/tmp.
2.  Identifica el ID i UID de l’usuari actual, de root i de guest.
# id root guest a221530tr 
3.  Llista els usuaris que han fet login en el sistema.
# who
	a221530tr :1           2022-10-24 08:07 (:1)
	a221530tr tty3         2022-10-24 09:23 ( esta oberta a una consola de text)
	
4.  Identifica el funcionament de l’ordre finger i executa-la.
# finger
	Login     Name       Tty      Idle  Login Time   Office     Office Phone
	guest     Calboo    *tty4       56  Nov  1 17:23 1          608424783
	root      root      *tty3       21  Nov  1 17:58
	trenau    trenau     tty2     1:32  Nov  1 16:48 (tty2)

5.  Fes l’ordre finger per al teu isx, per a root i per a guest.
# finger trenau
	Login: trenau         			Name: trenau
	Directory: /home/trenau             	Shell: /bin/bash
	On since Tue Nov  1 16:48 (CET) on tty2 from tty2
	   1 hour 28 minutes idle
	Last login Tue Nov  1 17:16 (CET) on tty3
	No mail.
	No Plan.

6. Llistar els shells disponibles en el sistema: /etc/shells.
# cat /etc/shells 
7. Idetifica quin shell tens assignat.
# finger [usuari]
8. Modificar les dades de l’usuari guest:
   1. Establir un nom, empresa i telèfon.
# chfn guest  
   2. Assignar-li el shell sh.
#usermod -s /bin/bash guest 

   3. Per a què servei el fitxer .plan
   
Segona part ---------------------------------------------------------------------------------------
1.  Fer actiu el directori home de l’usuari.
2.  Realitza l’ordre quaota i identifica la informació que mostra.
3.  Calcula l’ocupació d’espai del teu home amb l’ordre du, du -s, du -c.
# du 
 	( tota la info del emmagatzematge) 
# du -s 
	( total) 
4.  Llista l’ocupació del directori /var/tmp/esport. Calcula el total d’ocupació d’aquest directori.
# du /var/tmp/esport
	4	/var/tmp/esport

5.  Ídem mostrant els subtotals de cada sots directori que conté (/var/tmp/esport). És a dir, mostrar un nivell de profunditat.
# du -d 1 /var/tmp/esport/

6.  Fer el mateix del directori home de l'usuari.
# du -d 1

7.  També fer-ho de l’arrel del sistema.
# du -hd 1 /
8.  Lllistar l'ocupació de disc del directori home l'usuari agrupant fins a dos nivells de profunditat.
# du -d 2 
9.  Mostra l’ocupació d’espai del disc amb df -h.
# df -h

10.  Mostra l’ocupació d’espai del disc de les particions ext4.
# wheris ext4
	ext4: /usr/share/man/man5/ext4.5.gz
# df -h /usr/share/man/man5/ext4.5.gz
	Filesystem      Size  Used Avail Use% Mounted on
	/dev/nvme0n1p5   58G  9.5G   45G  18% /


Tercera part---------------------------------------------------------------------------------------

1.  Fer actiu el directori /var/tmp.
2.  Realitza l'ordre updatedb i esbrina què fa.
# sudo updatedb 
	(base de dades qe crea el pc de tots el teus archius memoritzanlos)
3.  Localitza tots els fitxers de nom vmlinuz.
# locate vmlinuz
4.  Localitza el fitxer services.
# locate services
5.  Localitza els fitxers de nom ifcfg.
# locate ifcfg
6.  Localitza el fitxer carta03. Ho fa? Perquè?
# locate carta03, no, no existe  
7.  Identifica el funcionament de l’ordre find i troba tots els fitxers de més de 1M del directori /boot.
# find /boot/ -size 1M
8.  Amb l’ordre find i troba tots els fitxers de menys de 4M del directori /boot. 
# find /boot/ -size 1M

9.  Amb l’ordre find i troba tots els fitxers de 1M a 4M del directori /boot.
# 
10.  Localita de /var/tmp tots els fitxers de propietat teva.
11.  Localita de /var/tmp tots els fitxers de propietat de root.
12.  Identifica els fitxers de /boot de nom initrd, amb find.
13.  Identifica els fitxers de /boot de nom .cfg amb find.
14.  Usant find llista els fitxers de tipus directori de /etc i de /tmp i de /boot.
# find /etc /tmp/ /boot/ -type d -ls
15.  Usant find llista els fitxers de tipus. link de /etc/systemd/system i de /tmp
# find /etc/systemd/system/ /tmp/ -type l -ls 2> /dev/null | head
16.  Usant find llista els fitxers de tipus file del teu home.
17.  Listar tots els fitxers del directori /var/tmp més nous que una data en concret.
18.  Listar tots els fitxers del directori /var/tmp amb data de fa deu dies fins ara.
19.  Listar tots els fitxers del directori /var/tmp amb data anterior a fa 72 hores.
Quarta part --------------------------------------------------------------------------------------
1.  Empaqueta el contingut de /var/tmp a un fitxer de /tmp anomenat paquet.tar.

2.  Llistar el contingut de paquet. Són rutes absolutes o relatives?
3.  Extreure el contingut de paquet dins de /tmp/newdata.
4.  Extreure només els fitxers txt i desar-los a /tmp/prova.
5.  Empaquetar el contingut de /tmp/newdata en un fitxer  anomenat paquet-absolute.tar al directori /tmp. Cal que el paquet es generi mantenint les rutes absolutes dels fitxers origens.
6.  Llistar el contingut de paquet. Són rutes absolutes o relatives?
7.  Esborrar el directori /tmp/newdata i tot el que conté.
8.  Desempaquetar paquet-absolute.tar  indicant com a destí /tmp/prova, però observar on es generen els elements desempaquetats. Aquí dins o al seu path absolut?
9.  Repatir l’exercici anterior provant les diferències de desenpaquetar amb l’opció -p o -P.
10.  Empaquetar el contingut de /var/tmp generant un paquet comprimit. Llistar el contingut i extreure’n només els fitxers txt.
11.  Empaquetar el contingut de /var/tmp generant un paquet comprimit amb bzip. Llistar el contingut i extreure’n només els fitxers txt.














