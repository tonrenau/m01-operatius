# 
﻿==============================================================================
Exercicis de fitxers (03)
﻿==============================================================================

Conceptes clau:
* Entrada de directori.
* Entrades: punt(.) i puntpunt (..)
* Metacaracters: Pathname Expansion. * ? [char…] [^char..] [!char…]
* Entrades ocultes: .name

Ordres a treballar:
ls, ls -la, ll, ls -d, ls -il, 
cd ., cd ..
.filename
wc, head, tail, less, file, sort

Exercicis
﻿==============================================================================
Primera part
1. Fer actiu el directori /var/tmp.
2. Llistar totes les entrades de directori (totes) del home de l'usuari.
3. Llistar totes les entrades de directori (totes) de l'arrel del        sistema.
4. Llistar amb ll el directori m01
5. Crear dins de m01 els fitxers:
   1. .secret.html
   2. .secretx.html
   3. .secret01.html
   4. .secret02.html
   5. .secret3.html
   6. .secret04.java
1. Llistar únicament els fitxers ocults del directori m01.
2. Llistar únicament els directoris del directori m01.
3. Quants enllaços durs té l'element . del directori m01?.
4. Quants enllaços durs té el directori /var/tmp?. Són els mateixos que del directori .. de m01?

Segona part
1. Fer actiu el directori /var/tmp.
2. Llistar els fitxers d’extensió txt de m01.
3. Compta quants n’hi ha (amb wc).
4. Llistar els fitxers ocults de m01 d’extensió html.
5. Llistar els fitxers ocults de m01 que comencin per .sec.
6. Llistar els fitxers de m01 (els ocults no9 que contenen un 0 en el nom.
7. Llistar els fitxers de m01 de nom carta i un dígit del 2 al 6.
8. Llistar els fitxers de m01 de nom carta i un dígit que no sigui ni el 3 ni el 4.
9. Llistar els fitxers ocults de m01 que s’anomenen .secret i dos caràcters més.
10. Llistar els fitxers ocults de m01 que s’anomenen .secret i dos dígits més.
11. Llistar els fitxers ocults de m01 que s’anomenen .secret i un dígit més.
12. Llistar els fitxers ocults de m01 que s’anomenen .secret i un caràcter més.
13. Llistar els fitxers ocults de m01 que s’anomenen .secret i una lletra més.

Tercera part
1. Fer actiu el directori /var/tmp.
2. Fer un llistat llarg paginat del directori actiu.
3. Fer un llistat llarg paginat del directori /tmp.
4. Mostrar els 15 primers elements del llistat del directori /tmp
5. Mostrar els 7 últims elements del llistat del directori /tmp
6. Mostrar els 5 primers elements del directori arrel.
7. Comptar quants elements llista el directori /tmp.
8. LListar només els noms dels elements de m01 ordenats.
9. Mosrat el contingut del fitxer /etc/passwd.
10. Mostrar el contingut del fitxer /etc/passwd ordenat.
11. Comptar els usuaris del sistema que hi ha al /etc/passwd.
12. Comptar els grups del sistema que hi ha al /etc/grups.

Quarta part
1. Amb l’ordre whereis esbrina on hi ha les pàgines de manual i l’executable de l’ordre ls.
2. Aplica l’ordre file als fitxers del man de l’ordre ls.
3. Aplica l’ordre file al fitxer executable de l’ordre ls.
4. Aplica l’ordre file als fitxers ocults del directori m01.
5. Aplica l’ordre file als fitxers d’extensió txt del directori m01.
6. Aplica l’ordre file al /dev/tty01
7. Aplica l’ordre file al /dev/console
8. Aplica l’ordre file al /dev/sr0
9. Usant l’ordre zcat mostra el contingut del fitxer de man de l’ordre ls.
10. Localitza l’executable i el man de l’ordre fdisk.
11. Aplica l’ordre file al fitxer del man de l’ordre fdisk.
12. Mostra el fitxer del man de l’rdre fdisk amb cat.
13. Mostra el fitxer del man de l’rdre fdisk amb zcat.

Cinquena part
1. Aplica l’ordre updatedb i esbrina què fa.
2. Amb locate identifica on és el fitxer passwd.
3. Amb locate identifica on és el fitxer services.
4. Amb locate identifica on és el fitxer gpm.service.
5. Amb locate identifica on és el fitxer useradd.

