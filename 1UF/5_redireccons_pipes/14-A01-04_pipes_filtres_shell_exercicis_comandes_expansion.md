******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  Exercici 14: exercicis de comandes:
                     -- path
                     -- alias
                     -- valors de retorn
                     -- expansions
                     -- command substitution
******************************************************************************
------------------------------------------------------------------------
alias
------------------------------------------------------------------------
01) Mostrar els alies actualment definits
# alias 
02) Fer un alias anomenat meu-data que mostri la data i el calendari.


03) Fer una alias de la ordre uname de manera que sempre mostri tota la 
    informació possible.
    Observar amb which quina és l'ordre associada a uname ara.

04) Fer un alias de tree que llisti únicament els directoris.
    observar amb which l'ordre associada a tree.
# alias tree='tree -d'
""" 
type -a tree
	tree is aliased to `tree -d'
	tree is /usr/bin/tree
	tree is /bin/tree
"""
05) Definir un alias permanentment a l'usuari que s'anomeni quisoc i 
    mostri per pantalla el uid, gid i el n'hom d'usuari.
# vim ~/.bashrc
al final del vim:
# alias quisoc='echo $UID $GID $USER
06) Definir un alias permanentment per a tots els usuaris del sistema 
    anomenat sistema que mostra la data, la informació complerta de 
    uname i tota la informació descriptiva de la cpu.
# sudo vim /etc/bash.bashrc
al final del vim 
# alias sistema='date; uname -a; lscpu' 

07) Assigna el prompt un format que mostri la hora la ruta absoluta del directori actiu i el nom d'usuari 
man bash 
08) Assigna al promt el format in mostra el nom d'usuari, el host, el num. d'ordre i el numero en l'historic d'ordres. 

------------------------------------------------------------------------
PATH: ordres externes i internes
------------------------------------------------------------------------
07) Mostrar el PATH actualment definit
# echo $PATH
08) Eliminar el path (abans fer-ne una còpia a la variable OLDPATH) i 
    observar què passa en executar una ordre interna i una externa)
# OLDPATH=$PATH
# PATH=
	Or.interna --> si van (pwd, jobs...) $enable -a, 
	OR.externa --> NO van (date, man...) ordres de dins el bin
09) Llistar l'estat de les ordres internes per obsevar si estan 
    activades o desactivades.
# enable -a 
10) Mostrar la pàgina de manual de les ordres internes
# man builtins
11) Mostrar l'ajuda de la ordre interna fg.

12) Crear una versió pròpia de l'ordre cal (per exemple que faci un ls), 
    i assignar-li precedència respecte a l'ordre del sistema. És a dir,
    fer que la versió de l'ordre de l'usuari s'executi en lloc de la del
    sistema.
#  
13) Crear una ordre pròpia local de l'usuari anomenada meu-data (en 
    realitat és la ròpia ordre date).

14) Elimina el directori local de l'usuari del PATH i executa l'ordre 
    meu-data. Què cal fer?
# PATH= 
# ./bin/meu-data
------------------------------------------------------------------------
Valors de retorn i compound commands
------------------------------------------------------------------------
15) Executa l'ordre free i comprova el valor de retorn d'execució.
# free -h
# echo $? 
	0 --> (correcte)
16) Executa l'ordre du -s i comproba el valor retornat.
# du -s
# echo $?
	0
17) Executa l'ordre date --noval i observa el codi de retorn.
# date --noval --es-van-deixar-guanyar
# echo $?
	1 --> (error)
18) Executa l'ordre nipum-nipam i observa el valor de retorn.
# nipum-nipam
# echo $?
	127
19) Crear dins de tmp un directori de nom dirprova. Únicament si l'ordre
    funciona mostrar per pantalla un missatge dient "ok directori creat"
# mkdir /tmp/test && echo "ok dir creat"
	ok dir creat
20) Crear dins de tmp un directori de nom dirprova. Únicament si l'ordre
    falla mostrar per pantalla el codi d'error de l'ordre i un missatge
    dient "ERR: el directori no s'ha pogut crear".
# mkdir /tmp/test1 ||  echo "error dir. no creat"
    
------------------------------------------------------------------------
Expansions
------------------------------------------------------------------------
21) Mostra el resultat de multiplicar 10 per 15 i restar-n'hi 5
# echo  $((10*15-5))
	145
22) Assigna a la variable num el valor de dividir 40 entre 4?
# num= echo $((40/4))
23) Assigna a la variable num el valor equivalent al doble del uid de 
    l'usuari
# num= echo $((UID*2))
24) Assigna a la variable CASA el home de l'usuari usant tilde expansion.
# casa=~a221530tr
25) Mostra el home de l'usuari root i el de l'usuari pere
# casa ~a221530 ~root
26) Assigna a la variable FITS el nom de tots els fitxers del directori 
    actual
# FITS=/tmp/prova/*
27) Assigna a la variable FITS el nom de tots els fitxers acabats en txt
    del directori actual.
# FITS=/tmp/prova/*.txt
28) Mostra per pantalla els noms noma, nomb, nomc, nomd
# echo nom{a..d}
29) Crear els directoris /tmp/dirprova/dir1 fins a dir10 de cop.
# mkdir -p /tmp/prova/noms{1..10}
30) Crear dins de /tmp/dirprova els següents fitxers buits: carta-1.txt,
    carta-a.txt...fins a carta-e.txt, carta-10.txt... fins a carta-15.txt.
# touch  carta-{1,{a..e},{10..15}}.txt
30) Mostrar per pantalla els següents noms: informe1.txt, informe5.txt, 
    carta-a.txt...fins a carta-e.txt, treball.inf, dossier10... fins 
    dossier22.


------------------------------------------------------------------------
Command substitution
------------------------------------------------------------------------
31) Assigna a la variable DATANOW la hora actual.
# DATANOW=$(date)
# echo $DATANOW
	Mon 16 Jan 2023 08:59:25 AM CET
32) Assigna a la variable la data en el moment en que es visualitza.
# DATANOW=$(date +%Y-%m-%d/%H:%M)
# echo $DATANOW 
	2023-01-16/08:57
33) llista el nom del paquet al que pertany l'ordre executable date. 
# sudo which date
# file $(ls /bin/date)
34) Executa un finger de l'uauri actual usant whoami.

35) Executa l'ordre que indica el tipus de fitxer que són cada un dels 
    executables del paquet al que pertany l'ordre fsck

36) Assihna a la variable FORMATS el nom dels fitxers executables que
    permeten formatar sistemes de fitxers.

    

