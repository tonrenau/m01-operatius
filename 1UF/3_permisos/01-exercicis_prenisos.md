# 
﻿Exercicis de permisos (01)
	Conceptes clau:
* Permisos bàsics. 
* Elements d’una entrada de directori
* Diferència entre fitxers i directoris.
* Chmod octal i simbòlix
* Chown / chgrp
* Umask


* Permisos avançats fitxers
* Permisos avançats directoris
Ordres a treballar:
ls -i 
stat
chmod
chown
chgrp
umask
Exercicis
________________
Primera Part
1. Escriu l’ordre que llista el nom i inode dels elements del directori actiu.
# ls -i

2. Quina informació hi ha realment desada en una entrada de directòri?
# Hi ha el nom i l'endtada de inode

3. Descriu que és un inode.
# Un numero que fa referencia i identifica i emmagetzema la serie de atributs una entrada  

4. Mostra la informació tècnica (contingut de l’inode) del fitxer carta.txt del directori actiu.
#stat carta.txt
	
5. Escriu en octal i simbòlic els tres casos clàssics de permisos a fitxers.

# chmod 640 operatius/apunts/carta.txt 		-rw-r----- 

6. Escriu en octal i simbòlic els tres casos clàssics de permisos a directoris.

# 750 -> rwx r-x ---


Segona Part------------------------------------------------------------------------------------

7. Des de /var/tmp estableix en octal al fitxer /tmp/carta.txt permisos de lectura i escritura per tothom.

# chmod 666 /tmp/m01/operatius/apunts/carta.txt 
	-rw-rw-rw- 1 a221530tr hisx1 155 Nov 21 08:17 /tmp/m01/operatius/apunts/carta.txt

8. Des de /tmp estableix en octal al directori /var/tmp/operatius permisos de lectura i escritura per tothom.

# chmod 666 /var/tmpoperatius 
9. Des de /vat/tmp estableix en octal al fitxer /var/tmp/informe.pdf permisos totals al propietari, de lectura pel grup i cap per a altres.

# chmod 640 informe.pdf

10. Des del directori /var/tmp estableix en octal al directori /var/tmp/operatius permisos totals al propietari, de lectura al grup i cap a altres.
# chmod 640 operatius

11. Eliminima del fixer executable /tmp/practica.py totes les x que tingui. Des del teu home.
# chmod  -x /tmp/practica.py 
12. Estableix al directori /vat/tmp/operatius que els permisos de others siguin els mateixos que els que té per a group. El directori actiu és /tmp.
# chmod g=o operatius/
	drwxr-xr-x

Tercera Part-----------------------------------------------------------------------------------

13. L’usuari ton propietari del fitxer /tmp/carta vol que aquest fitxer passi a ser propietat de l’usuaria guest. Des del directori actiu /tmp.

# sudo chown guest operatius/apunts/carta.txt 
	-rw-rw-rw- 1 guest hisx1 155 Nov 21 08:17 operatius/apunts/carta.txt
14. L’usuari ton vol que el directori /var/tmp/operatius (del qual n’és propietari) passi a ser del grup practica (que també en forma part).
# sudo chown .pratica operatius/apunts/carta.txt 
	-rw-rw-rw- 1 guest pratica 155 Nov 21 08:17 operatius/apunts/carta.txt
15. Si la màscara de permisos és 002 quins són els permisos per defecte de directoris.
# 775 osigi usuari i grup tot, others no poden escriure.

16. Si la màscara de permisos és 002 quins són els permisos per defecte de fitxers?.

17. Estableix la màscara per defecte per tal de que el propietari tingui accés total, el grup de lectura i a altres cap permís.







Quarta  Part-----------------------------------------------------------------------------------
18. Activa al directori /tmp/master el setgid. Directori actual el teu home.
# chmod g+s ..
# ls -la
	drwxr-sr-x  2 a221530tr project  4096 Nov 24 12:55 .

19. Des del teu home estableix el setuid al fitxer executable /tmp/practica.py permeten l’accés total al propietari, execució al grup i a altres.
# chmod 4777 /tmp/prac.py
	-rwsrwxrwx 1 a221530tr hisx1 703 Nov 28 08:11 prac.py

20. Què fa el setuid en un directori?
# El setuid en un directori no serveix per res 

21. Estableix al directori /tmp/compartit (des del directori /tmp) els permisos per fer d’aquest directori un espai compartit per tothom però amb protecció contra esborrat d’altres.
# chmod 1777 compartit/
	
	llavorens desde l'usuari guest aven ficat el sticky abans, llavorens al voler eliminar un arxiu no es posible.
	

22. Si els permisos que establim en octal a un fitxer són 1766 com és representen en simbòlic?
# rwx rw- rwT

23. Si els permisos que establim a un fitxer són 6765 com és repersentem en simbòlic?
# rws rwS r-x 




















