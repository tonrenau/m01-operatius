Exercicis de processos:
tasques puntuals / periòdiques
[ at ]

1. Verifica que el servei at (atd) està activat. Si no ho està o no està instal·lat
instal·lar-lo i activar-lo.
# systemctl status atd
2. Executa d’aqui tres dies la tasca: generar a /tmp/info.log el resum de la informació:
uname -a, uptime i ps ax.
# at now +3days 
	warning: commands will be executed using /bin/sh
	at> uname -a >> /tmp/info.log
	at> uptime >> /tmp/info.log
	at> ps ax >> /tmp/info.log
	at> <EOT>
	job 14 at Sun Dec 18 11:36:00 2022

3. Executa demà a les 14:00 la tasca puntual que genera a /tmp/info.log la següent
informació: date, who, uptime i uname -a.
# at 14:00 tomorrow
4. Executa del dia 25 de desembre d’aquest any al migdia les tasques contingudes en
el fitxer tasques.
# at 12:00 Dec 25 -f tasques 
5. Llista totes les tasques puntuals planificades.
# at -l // # atq
6. Llista el directori on hi ha les tasques programades. A què correspon cada tasca.
# sudo ls -l /var/spool/cron/atjobs/
total 24
-rwx------ 1 a221530tr daemon 3924 Dec 15 11:38 a0000e01a90c7c
-rwx------ 1 a221530tr daemon 3877 Dec 15 11:50 a0001201a8fc2c
-rwx------ 1 a221530tr daemon 3877 Dec 15 11:51 a0001301a9014b
-rwx------ 1 a221530tr daemon 3925 Dec 15 11:53 a0001401a901cc
-rwx------ 1 a221530tr daemon 3864 Dec 15 11:58 a0001701a93124
-rwx------ 1 a221530tr daemon 3851 Dec 15 11:58 a0001801a933f4

7. Mostra el fitxers que conté la planificació que at ha generat de la primera de les
tasques puntuals programades. Identifica els següents elements:
# sudo vim /var/spool/cron/atjobs/a0000e01a90c7c
● Shell --> /bin/sh
● Usuari i grup --> a221530tr daemon 
● Email al que s’envia la sortida --> mail a221530tr 
● Llistat de les ordres a fer. --> al final de tot del script 
● Tota la informació que hi ha a més a més a què correspon? 

8. Elimina la primera de les tasques planificades.
# atrm 14
9. Planifica que el sistema executi les tasques contingudes en el fitxer tasques quan
‘pugui’.
# batch < tasques
10. Mostra quin és l’estat actual de càrrega del sistema.
# htop // # uptime
11. Elimina totes les tasques periòdiques programades.
# atrm 20 18 23 19 24

[ cron ]

12. Verifica que el servei cron està actiu. Si no ho està o no està instal·lat instal·la’l i
activa’l.
# system status cron
13. Identifica quin és el fitxer del dimoni (servidor) del cron.

14. Quin és el fitxer que permet planificar tasques periòdiques per a tot el sistema
(system wide)? Qui és l’usuari que el pot editar?

15. Quina diferència hi ha entre la línia que configura una tasca periòdica en el fitxer
global del sistema i en la configuració particular d’un usuari?
# cat /etc/crontab
16. Anomena 4 directoris del sistema on es poden posar fitxers amb tasques que
s’executaran periòdicament segons el nom del directori usat.
# tree /etc/cron*
17. Genera una tasca periòdica cada dia laborable a les 8 del matí que genera el fitxer
/tmp/users.log amb el llistat dels usuaris connectats al sistema.
# crontab -e
 0 8 * * 1-5 date >> /tmp/users.log
18. Genera una tasca periòdica que cada 10 minuts genera un registre a
/tmp/heartbeat.log amb la data. Ha d’estar actiu de les 8 del matí a les dues, tots els
laborables del mesos de genear a juny.

19. Com a root programa l’execució del fitxer backup.sh (no existeix ara) tots els
divendres a la nit (de divendres a dissabte) passats 20 minuts de mitjanit. S’ha de fer
un més si, un més no.

20. Llista el directori que té les tasques periòdiques programades. A què correspon cada
element que hi ha? Descriu-ho.


[ keywords ]

21. Programa que cada vegada que el sistema s’ha reiniciat es generi el fitxer
/var/tmp/reinicis.log amb la data.
# @reboot date >> /tmp/
22. Programa que cada dia laborables a les 14h es generi un fitxer /var/tmp/apagat.log
amb la data i s’apagui automàticament l’equip.

[ allow / deny ]

23. Configura el servei at perquè únicament l’usuari prova01 pugui generra tasques
programades puntuals. Demostra que el teu usuari no pot i prova01 si.

24. Configura el servei at perquè l’usuari prova01 estigui vetat i no pugui realitzar
tasques puntuals i tots els altres usuaris sí. Demostra-ho.

25. Per a què serveixen els fitxers cron.allow i cron.deny. Explica-ho.

26. Elimina totes les tasques periòdiques programades.
# crontab -r
