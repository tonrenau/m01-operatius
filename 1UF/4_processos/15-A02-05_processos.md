******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A02-05-processos
  Exercici 15: exercicis de processos i tasques automatitzades
                     -- prcessos i PID
                     -- tasques en segon pla: jobs
                     -- tasques automatitzades
******************************************************************************
------------------------------------------------------------------------
Processos
------------------------------------------------------------------------
01) Mostrar tots els processos del sistema.
# ps -ax

02) Mostrar tot l'arbre de processos incloent el pid i la ordre.
# pstree -pal 
    
03) Prova les ordres: ps, ps a, ps x, ps ax, ps -fu pere, ps -Fu pere.

04) Llistar els processos  fent un llistat llarg. el PID i el PPID.
# ps -l

05) Entrar en un subshell i fer un llistat llarg dels processos. 
    Identificar el PID del procés pare del shell actual.
# bash
# ps -l

06) Identifica el PID del procés init usant l'ordre pidof.

07) identifica el pid del servei d'impressió cupsd amb l'ordre pidof.

08) Usant l'ordre pgrep llista els processos de l'usuari root.

09) usant l'ordre pgrep localitza el procés init.

10) Utilitzant l'ordre fuser per saber quins processos utilitzen el directori /tmp.

11) Llista tots els senyals generables amb l'ordre kill.

12) Genera un procés sleep 1000 i mata'l amb kill.

13) Mata el bash actual.

14) Llista tots els processos mingetty i mata'ls de cop tots usant u
    na sola ordre kill.

15) genera 3 processos sleeep 10000 i mata'ls tots de cop usant killall.


------------------------------------------------------------------------
Jobs
------------------------------------------------------------------------
16) executa tres ordres sleep en segon pla i llista els treballs.

17) Inicia l'edició d'un fitxer amb vi i deixa'l suspès d'execució en segon pla. Mostrar els treballs.

18) Mata el segon dels treballs (un sleep)

19) Passa a primer pla el primer dels treballs (un sleep), i mata'l amb ctrl+c.

20) Passa a primer pla el treball més recent. Quin és. Acabar.
    
21) Llistar tota l'estructura de directoris partint de l'arrel. Que no es generin missatges d'error i enviar la sortida a null (no volem desar res és només per fer-lo treballar!). Un cop iniciat aturar el proces. Llistar els treballs.


22) Reanudar l'execució del tree anterior en segon pla.


------------------------------------------------------------------------
Examinar l'Estat 
------------------------------------------------------------------------
23) Executar l'ordre que monitoritza els processos. Llistar-los per prioritat.

24) Executar l'ordre vmstat. Descriu almenys tres dels elements dels que informa.

25) Executar l'ordre free i descriure la informació que mostra.

25) Digues quanta estopna fa que el sistema està engegat ininterrumpudament.

26) disgues quina versió de kernel i de harware s'està utilitzant.
    
------------------------------------------------------------------------
tasques periòdiques
------------------------------------------------------------------------
27) Executa d'aquí cinc minuts l'odre date, w, i finger (totes de cop) 

28) Executa a les xx en punt l'ordre who.

29) Executa quan la càrrega del sistema ho permerti un llistat de tots els directoris de home. Envia el llistat a /dev/null (per no ocupar). Acaba el llistat amb el missatge FI.

30) Executa cada 5 minuts l'ordre nmap localhost, també cada hora (en el minut 15) dels dilluns i els divendres l'ordre df -h.

