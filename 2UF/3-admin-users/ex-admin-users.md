# Preguntar  14,17 

# 1. Crear l’usuari user1 i observar els valors per defecte que pren de shell, home, uid, i gid. Mostrar la línia amb la informació d’usuari del fitxer de comptes d’usuari. S’ha creat algun grup? Mostrar la línia d’informació del fitxers de comptes de grup.
useradd user1 
tail -1 /etc/passwd
    user1:x:1001:1001::/home/user1:/bin/bash
tail -1 /etc/group
    user1:x:1001:
# 2. Crear els grups: pàdel, quimica i alumnes.
root@debian:~# groupadd padel 
root@debian:~# groupadd quimica
root@debian:~# groupadd alumnes
# 3. Crear l’usuari user2 que tingui el shell /bin/sh, que pertany i al grup pàdel (primari), al grup alumnes de secundari i que tingui l’UID 2000. Mostrar la seva informació.
root@debian:~# useradd -s /bin/sh -N -g padel -G alumnes -u 2000 user2
root@debian:~# tail -1 /etc/passwd
user2:x:2000:1002::/home/user2:/bin/sh
# 4. Modificar l’usuari user2 assignant-li també els grups secundaris quimica i users.
root@debian:~# usermod -aG quimica,users user2 
root@debian:~# id user2
uid=2000(user2) gid=1002(padel) groups=1002(padel),100(users),1003(quimica),1004(alumnes)
# 5. Eliminar el grup pàdel. Es pot? Perquè? (si no es pot no l’elimineu).
No es pot, ja que es grup principal de user2
# 6. Modificar l’usuari user2 de manera que el seu grup principal passi a ser alumnes. Observar on està desada la informació del grup principal i del grups secundaris als que pertany l’usuari.
root@debian:~# usermod -g alumnes -aG padel user2
root@debian:~# id user2
uid=2000(user2) gid=1004(alumnes) groups=1004(alumnes),100(users),1002(padel),1003(quimica)
# 7. Modificar l’usuari user2 afegint el grup pàdel com a grup secundari.
usermod -aG padel user2
# 8. Eliminar el grup pàdel. Ara es pot?. Perquè?
root@debian:~# groupdel padel
Si, pq ara no hi ha ningun usuari amb el grup padel com a principal.
# 9. Modificar que el shell de l’usuari user2 sigui el /bin/bash.
root@debian:~# usermod -s /bin/bash user2
root@debian:~# tail -1 /etc/passwd
user2:x:2000:1004::/home/user2:/bin/bash
# 10. Eliminar l’usuari user2 i l’usuari user1 (usuari i home)
userdel -r user1
userdel -r user2
# 11. Crear l’usuari user1 establint el seu home a /tmp/develop/projecte. Cal assegurar-se que el directori home de l’usuari es creï si no existeix.
root@debian:~# useradd -m -d /tmp/develop/projecte user1
root@debian:~# tail -1 /etc/passwd
user1:x:1001:1001::/tmp/develop/projecte:/bin/bash
# 12. Crear l’usuari user2 establint el seu home dins del directori base /tmp. Cal que el home de l’usuari es creï si no existeix.
root@debian:~# useradd -m -b /tmp/ user2
root@debian:~# tail -1 /etc/passwd
user2:x:1002:1002::/tmp//user2:/bin/bash
# 13. Quina diferència hi ha entre basename i dirname en les opcions de useradd?.
-b crea el home en la ruta indicada 
-d sera la ruta absulutadel home d'aquell usuari 
# 14. Crear un directori d’esquelet personalitzat anomenat skel_alum. Posar dins la xixa que cregueu pertinent.
root@debian:~# ls -la /etc/skel_alum/
total 32
drwxr-xr-x   2 root root  4096 Mar 17 12:56 .
drwxr-xr-x 139 root root 12288 Mar 17 12:53 ..
-rw-r--r--   1 root root    11 Mar 17 12:56 alums
-rw-r--r--   1 root root   220 Mar 17 12:55 .bash_logout
-rw-r--r--   1 root root  3526 Mar 17 12:55 .bashrc
-rw-r--r--   1 root root   807 Mar 17 12:56 .profile
# 15. Crear l’usuari user3 usant l’esquelet skel_alum.
useradd -m -k /etc/skel_alum user3
# 16. Elimina els usuaris i els homes dels usuaris user1, user2 i user3.
root@debian:~# userdel -r user1
userdel: user1 mail spool (/var/mail/user1) not found
userdel: user1 home directory (/tmp/develop/projecte) not found
root@debian:~# userdel -r user2
userdel: user2 mail spool (/var/mail/user2) not found
userdel: user2 home directory (/tmp//user2) not found
# 17. Estableix que per defecte el shell dels nous usuaris sigui /bin/sh, el grup alumnes i l’esquelet skel_alum. Observa que estiguin correctament aquests valors definits per defecte. De quines maneres es pot observar?.
useradd -D -s /bash/sh -g alumnes -m -k /etc/skel_alum/
# 18. Crear l’usuari user1 i observar quins valors per defecte se li han aplicat.

# 19. Crear l’usuari user2 assegurant-se de que no se li crea un grup principal ‘egoísta’.
useradd -N -m user2
# 20. Eliminar els usuaris user1 i user2.
userdel user1
userdel user2
# 21. Restablir els valors per defecte usuals. Quins són?
/etc/default/useradd
useradd -D /bin/sh -g 
# 22. Quin fitxer conté els rangs de valors que s’associen als usuaris del sistema i als usuaris ordinaris? Quins són aquests valors?
/etc/passwd
# 23. Quin fitxer conté els valors per defecte de la política d’expiració de passwords?
/etc/login.defs
# 24. Crear l’usuari user1. Quin valor pren el camp password? En quins fitxers?
x --> /etc/passwd
! --> /etc/shadow
# 25. Assignar un password a l’usuari user1. Observar on ha quedat desat i en quin format.
passwd user1 

root@debian:~# tail -1 /etc/shadow
user1:$y$j9T$8mqBW4YMevyiQY3ZDJf0$n5oP1Upu70j8LkbHVZ6CaUI4ZQCB83ekNLNmXA3:19801:0:99999:7:::
# 26. Bloquejar l’usuari user1. Verificar que no pot iniciar sessió. Observar el format actual del password. Desbloquejar-lo i verificar que pot accedir i el format del password.
root@debian:~# passwd -l user1 
passwd: password expiry information changed.
root@debian:~# tail -1 /etc/shadow
user1:!$y$j9T$8mqB.W4YMevyi/QY3ZDJf0$n/x/5oP1Upu70j. 8LkbHVZ6CaUI4ZQCB83ekNLNmXA3:19801:0:99999:7::: 
<!-- A l'hora de bloquejarlo s'ha post un admiracio devant del pswd. -->

# 27. Fer que l’usuari user1 no requereixi de password per iniciar sessió. Verificar i observar el format de password desat. Assignar-li de nou un password.
passwd -d user1 

root@debian:~# passwd -S user1
user1 NP 03/19/2024 0 99999 7 -1
# 28. Establir com a política de password de l’usuari user1 que no pot modificar-se el password fins passats dos dies, l’ha de modificar cada 30 dies, amb 5 dies d’avís d’antelació i 3 dies de període de gràcia.
root@debian:~# chage -m2 -M30 -W5 -I3  user1

root@debian:~# chage -l user1
Last password change					            : Mar 19, 2024
Password expires					                : Apr 18, 2024
Password inactive			                		: Apr 21, 2024
Account expires					                  	: never
Minimum number of days between password change		: 2
Maximum number of days between password change		: 30
Number of days of warning before password expires	: 5

# 29. Mostrar amb les ordres chage i passwd la informació del compte d’usuari de user1. Mostrar els valors emmagatzemats en els fitxers dels comptes d’usuari, grups i passwords.

root@debian:~# tail -1 /etc/passwd
user1:x:1001:2::/home/user1:/bin/bash

root@debian:~# tail -1 /etc/shadow
user1::19801:2:30:5:3::

# 30. Eliminar l’usuari user1.
userdel -r user1
# 31. Assignar a tots els usuaris del sistema una variable anomenada ESCOLA amb el valor “escola del treball de barcelona” i una anomenada CURS amb el valors “ASIX”.
root@host:/# vim /etc/profile
pere@host:~$ echo $escola 
EDT bcn
pere@host:~$ echo $curs
Asix
# 32. Assignar de manera permanent al teu usuari una variable anomenada NOM amb el teu nom i cognom i redefinir la variable CURS amb el valor “hisx1”.

# 33. Verificar que l’usari guest pot mostrar el valor de les variables ESCOLA i CURS i contrastar amb els valors del teu usuari de les variables ESCOLA, CURS i NOM.

# 34. Assignar a tots els usuaris del sistema una funció anomenada sessiontime que usant la variable SECONDS calcula el número de minuts que una sessió shell porta engegada. Definir un alias anomenat aniversarique mostri el calendari del més del naixement de l’Stallman.
root@debian:~# vim .bashrc 
    -> a dintre escrivim: alias cumple='cal 10 2004'

root@debian:~# cumple
    October 2004      
Su Mo Tu We Th Fr Sa  
                1  2  
 3  4  5  6  7  8  9  
10 11 12 13 14 15 16  
17 18 19 20 21 22 23  
24 25 26 27 28 29 30  
31 
# 35. Redefinir pel teu usuari l’alias aniversaride manera que mostri el més del teu aniversari.

# 36. Contrastar la utilització de la funció sessiontimei l’àlies aniversarientre els usuaris guest i el teu usuari.




