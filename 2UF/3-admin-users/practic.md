# LPIC-1


## 107.1 Manage user and group accounts and related system files

### Exercices


[users]

# 1. Create user pere using /bin/sh as shell, uid 2020, primary group users, secondary group sudo and bin.
useradd -s /bin/sh -u 2020 -N -g users -G sudo,bin pere
# 2. Create directory /tmp/project. Create user marta using primary group users and home directory /tmp/project.
mkdir /tmp/project
useradd -m -d /tmp/project/ -N -g users marta
# 3. Define as default user options home primary group bin  and /bin/bash as shell. Then create user anna.
useradd -D -g bin -s /bin/bash 
useradd anna
# 4. Modify user anna assigning /bin/sh shell, primary group users, append secondary groups sudo and games.
usermod -s /bin/sh -N -g users -aG sudo,games anna
# 5. Create an pue skeleton and create a new user called jan using this skeleton.
useradd -m -k /etc/skel_pue jan
# 6. Revert the changes in exercise 3.

# 7. Delete users and home directories created in the previous exercises.
userdel -r pere
userdel -r marta

[groups]

# 8. Create group *dads*
groupadd dads
# 9. Create group *moms*
groupadd moms
# 10. Create user pere with primary group dads and secondary groups users and moms.
useradd -g dads -G users,moms pere
# 11. Modify group dads, change the name and the gid.
usermod -n papa -g 2000 dads
# 12. Delete the group moms
groupdel moms
# 13. Delete the group dads. Do whatever is necessary…
userdel -r pere 
groupdel dads

[password policies]

# 14. create user pere without password (password-less)
useradd -N -m pere
passwd -d pere 
# 15. lock the user pere
passwd -l pere      
# 16. unlock the user pere.
passwd -u pere
# 17. user pere should change the password in the next login.
passwd -e pere
# 18. user pere account expires in three days.
chage -E 2024-03-16 pere
# i 19 dies
chage -E $(date -d +19days +%Y-%m-%d) pere
