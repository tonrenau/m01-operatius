Exercicis PER (01)
Conceptes clau:
❏grep
❏tr	
❏cut
❏sort
❏sed

pregunatr el 31

# [Grep]

# 1. Del resultat de fer un ​ head ​ de les 15 primeres línies del fitxer ​ /etc/passwd ​ mostrar les línies que ​ contenen ​ un ​ 2 ​ en algun lloc.
head -n15 /etc/passwd | grep "2"
# 2. Del resultat de fer un ​ head ​ de les 15 primeres línies del fitxer ​ /etc/passwd mostrar només les línies que tenen ​ uid ​ ​ 2 .
head -n15 /etc/passwd | grep "^[^:]*:[^:]*:2:"
# 3. Usant ​ grep ​ valida si un ​ dni​ té el format apropiat.
echo 47751439N | grep -E "[0-9]{8}[A-Z]" 
# 4. Usant ​ grep ​ valida si una ​ data​ té el format ​ dd-mm-aaaa . 
 echo "29-01-2024" | grep -E "^[0-9]{2}-[0-9]{2}-[0-9]{4}$" 
# 5. Usant ​ grep ​ valida si una ​ data​ té el format ​ dd/mm/aaaa . ​
echo "29/01/2024" | grep -E "^[0-9]{2}/[0-9]{2}/[0-9]{4}$" && echo "OK"
# 6. Usant ​ grep ​ validar si una ​ data​ té un format ​ vàlid ​ . Els formats poden ser: dd-mm-aaaa ​ o ​ dd/mm/aa ​ .
echo "29/01-2024" | grep -E "^[0-9]{2}[/-][0-9]{2}[/-][0-9]{4}$"
# 7. Buscar totes les línies del fitxer ​ /etc/group ​ que tenen la cadena “​ bin ​ ” o “”​ adm ​ ”.
grep -E "bin|adm" /etc/group 


# [tr]

# 8. Mofidicar el delimitador de ​ /etc/group ​ per un ​ tabulador . ​
tr ":" "\t" < /etc/group
# 9. LListar les deu primeres línies del ​ /etc/passwd ​ en ​ majúscules ​ .
head -n10 /etc/passwd | tr "[a-z]" "[A-Z]" 
# 10. Codificar les deu últimes línies del fitxer ​ /etc/group ​ substituint les vocals per números​ .
tail -n10 /etc/group | tr "aeiou" "12345" 
# 11. Normalitzar el fitxer ​ /etc/fstab ​ (sense comentaris) a un ​ tabulador​ de separador.
tr -s "[:blank:]" "\t" < /etc/fstab
# 12. Normalitzar el llistat del directori ​ arrel ​ a un ​ espai ​ de delimitador.
ls -l / | tr -s "[:blank:]" " " 
# 13. Normalitzar el fitxer ​ oficinas.dat ​ a “​ : ” ​ com a delimitador.
tr -s "[:blank:]"  ":" < ofi
# 14. Del fitxer ​ /etc/group ​ modificar el delimitador “​ : ​ ” posant-hi el delimitador “​ -- ​ ”.
sed "s/:/--/g" /etc/group 




# [Cut]

# 15. Retallar del caràcter 2 al 10 d'un llistat llarg de l’arrel.
ls -l / | cut -c2-10
# 16. Del llistat de l’arrel (normalitzat) retallar els camps ​ type/permisos ​ i ​ proprietari ​ .
ls -l | tr -s "[:blank:]" "." | cut -d. -f1,3
# 17. Del fitxer /etc/fstab (normalitzat i sense comentaris) retallar els camps ​ device	(primer) i ​ fstype ​ (tercer).
tr -s "[:blank:]" ":" < /etc/fstab | cut -d: -f2,4
# 18. Del fitxer /etc/passwd retallar el ​ login ​ , uid ​ i ​ shell ​ 
# 19. Llista el fitxer ​ /etc/group ​ ordenat per ​ gname ​ en ordre descendent.
sort -r /etc/group
# 20. Llista el fitxer ​ /etc/group ​ ordenat per ​ gid ​ en ordre ascendent.
sort -t: -k3n 
# 21. Llista el fitxer​ /etc/passwd ​ ordenat per ​ gid ​ i ​ uid ​ en ordre ascendent.
head /etc/passwd  | sort -t: -k4n -k3
# 22. Llista el fitxer /etc/passwd ordenat per ​ gid ​ (descendent) i ​ login ​ en ordre ascendent.
head /etc/passwd  | sort -t: -k4nr -k1
# 23. Llistar el fitxer ​ /etc/fstab ​ ordenat per ​ fstype ​ (tercer camp).
# 24. Llistar el ​ login ​ , ​ uid ​ i ​ shell ​ del fitxer ​ /etc/passwd ​ ordenat per ​ gid . ​
cut -d: -f1,3,7 /etc/passwd | sort -t: -k4n
# 25. Llistar (llistat llarg) ​ l’arrel ​ ordenat per ​ size ​ .
ls -l / | tr -s "[:blank:]" "." | sort -t. -k5
# 26. Llistar (llistat llarg) els ​ type/permisos ​ , ​ propietari ​ i ​ mida ​ de ​ l’arrel ​ ordenat per ​ mida ​ .
# 27. Llistar tots els ​ gids ​ (diferents) del fitxer​ /etc/passwd ​ ascendentment.
cut -d: -f4 mypwd | sort -t: -k4n|  uniq  
# 28. Mostra el ​ login​ , ​ uid​ i ​ gid​ de totes les línies del ​ /etc/passwd que no utilitzin el shell /bin/bash . ​ Mostrar-ho per ordre de ​ login . ​
cut -d: -f1,3,4,7 mypwd | grep -v "/bin/bash$"


# [sed]

# 29. Del fitxer ​ /etc/group ​ modificar el delimitador “​ : ​ ” posant-hi el delimitador “​ -- ​ ”.
sed "s/:/--/g" mypwd 
# 30. Llistar ​ /etc/passwd ​ substituint ​ root ​ per ​ LoRoot . ​
sed "s/root/LoRoot/g" mypwd
# 31. Llistar el fitxer ​ /etc/group ​ substituint el segon camp per ​ --secret-- ​ .
sed -r "s/(^[^:]*):[^:]*:/\1:--secret--:/g" /etc/group 

# Substituir el tercer camp per --uid--
sed -r "s/^([^:]*):([^:]*):[^:]*:/\1:\2:--uid--:/g" /etc/passwd 

# 32. Substituir del fitxer​ /etc/passwd ​ tots els shells ​ /bin/bash ​ per ​ /bin/sh . ​
sed "s/\/bin\/bash$/\/bin\/sh/g" mypwd 
# 33. Substituir del fitxer ​ /etc/passwd ​ tots els shells​ /bin/bash ​ per ​ /bin/sh ​ , però només en els usuaris del ​ gid 0 . ​
grep "^[^:]*:[^:]*:[^:]*:0:" mypwd | sed "s/\/bin\/bash$/\/bin\/sh/g"
# 34. Substituir del fitxer​ /etc/passwd ​ totes les aparicions de la paraula ​ bin ​ per ​ BIN ​ des de la línia 1’ fins el final.
sed "1,$ s/bin/BIN/g" mypwd 
# 35. Substituir del fitxer​ /etc/passwd ​ totes les aparicions de la paraula ​ bin ​ per ​ BIN ​ des del mig del fitxer fins el final. **difícil!**
 sed "$(($(cat /etc/passwd | wc -l)/2)),$ s/bin/AMO/g" /etc/passwd
# 36. Llistar el ​ login ​ i ​ uid ​ del fitxer ​ /etc/passwd ​ amb el format ​ login(uid) ​ , ordenat per login.



# 37 fica a pere:barcelona, pere entre parentesis






# [Camps]

# 37. Feta l’assignació ​ “ gid=0 ​ ” , ​ llistar per ordre de ​ uid ​ els usuaris del ​ /etc/paswd ​ que pertanyin a aquest ​ gid . ​
grep "^[^:]*:[^:]*[^:]:0:" /etc/passwd | sort -t: -k3n
# 38. Feta l’assignació “​ log=”root ​ ”” llistar el ​ login , ​ ​ uid ​ i ​ gid ​ d’aquest ​ login ​ del fitxer /etc/passswd . ​
grep "^$log" /etc/passwd | cut -d: -f1,3,4
# 39. La variable “​ mat ​ ” conté el número de matrícula d’un alumne. Valida que té el format vàlid “​ AAA99 9999 ” ​ . Si és així ha de mostrar “​ Ok ​ ” per stdout.
echo $mat | grep -E "[A-Z]{3}[0-9]{2} [0-9]{4}" && echo ok
# 40. Si fem l’ordre “​ $ echo “a:b:c:a:d:e:a:a:f:g” | grep “^.*:a: ​ ”, quina és la sortida?. 
a:b:c:a:d:e:a:a:
# 41. Si fem l’ordre “​ $ echo “a:b:c:d:e:f” | sed “s/^a:b:c://g ” ​ , quina és la sortida?.
d:e:f
# 42. Llistar totes les línies d’un fitxer que són comentaris: entenem per comentaris que el primer caràcter no blanc és el #.
grep "^#" /etc/fstab 



