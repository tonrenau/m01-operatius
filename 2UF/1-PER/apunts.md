 
 ## man man
 Nivells del pc 

       1   Executable programs or shell commands
       2   System calls (functions provided by the kernel)
       3   Library calls (functions within program libraries)
       4   Special files (usually found in /dev)
       5   File formats and conventions, e.g. /etc/passwd
       6   Games
       7   Miscellaneous   (including   macro  packages  and  conventions),  e.g.  man(7),  groff(7),
           man-pages(7)
       8   System administration commands (usually only for root)
       9   Kernel routines [Non standard]



 ex: man man (1)
 ex: man 5 passwd (5)
 ex: passwd (1)
 ex: man sudo (8)

 sed -r "/^11\t/ s/Este/Norte/g" ofi 
 sed -i " s/Este/Norte/g" ofi 
 sed  "s/:[^:]*$/:0/g" myofi


 PER
 ? 0 o 1 vegada 
    ( 
    echo "a:bbbb:c:bb:e:bbb:f:b:g" | grep -E :b? 
    )

 + 1 o mes vegades = {1,}
 * 0 a n veagdes = {0,}
