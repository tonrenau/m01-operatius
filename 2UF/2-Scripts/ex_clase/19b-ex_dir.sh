#! /bin/bash
# Ton Renau
# prog dir
# 
# llistar el ls amb contador
#--------------------------------------------------

# 1 validar arg
ERR_ARG=0
ERR_NODIR=2
if [ $# -ne 1 ];then
    echo "ERROR: num arg no valid"
    echo "USAGE $0 dir"
    exit $ERR_ARG
fi

# validar que es un dir
dir=$1
if ! [ -d $dir ];then
    echo "ERROR: $dir no es dir"
    echo "USAGE: $0 dir"
    exit $ERR_NODIR
fi

# xixa
count=0
llista=$(ls $dir)
for elem in $llista
do
    echo "$count: $elem"
    ((count++))
done
exit 0