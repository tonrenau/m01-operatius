#! /bin/bash 
#  EDT ASIX 2023 
# març 16 
# sinopsis: prog dir
# -------------------------------------------------------------
# sinopsis: prog dir
# validar que es rep un argument i que es un directori i llistar-ne el contingut.
# per llistar el contingut amb un ls ja ni ha prou 

# validar argument
err_arg=1
err_dir=2
if [ $# -ne 1 ] 
then 
	echo "ERROR: numero args no valid"
	echo "USAGE: $0 dir"
	exit $err_arg
fi
#validar directori
dir=$1
if ! [ -d $dir ] 
then
	echo "ERROR: no es directori"
	echo "USAGE: $0 dir"
	exit $err_dir	
fi 
# dir que es  cada element del directori  
llista=$(ls $dir)
for elem in $llista
do 
	if [ -h "$dir/$elem" ]; then
	       echo "$elem es un link"
	elif [ -d "$dir/$elem" ]; then		
		echo "$elem es un directori"
	elif [ -f "$dir/$elem" ] ; then
		echo "$elem es un regular"
	else 
		echo "elem es una altre cosa"
	fi
done
exit 0

# contar elements de la llista
llista=$(ls $dir)
count=1
for elem in $llista
do 
	echo "$count.-$elem" 
	((count++))

done

exit 0 
