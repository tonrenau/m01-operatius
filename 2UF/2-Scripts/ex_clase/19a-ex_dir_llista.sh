#! /bin/bash
# Ton Renau
# prog dir
# rep un arg i es dir. i es llista
#--------------------------------------------------

# 1 validar arg
ERR_ARG=0
ERR_NODIR=2
if [ $# -ne 1 ];then
    echo "ERROR: num arg no valid"
    echo "USAGE $0 dir"
    exit $ERR_ARG
fi

# validar que es un dir
dir=$1
if ! [ -d $dir ];then
    echo "ERROR: $dir no es dir"
    echo "USAGE: $0 dir"
    exit $ERR_NODIR
fi

# xixa
ls $dir 
exit 0