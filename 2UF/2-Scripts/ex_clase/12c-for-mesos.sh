#! /bin/bash
# Ton Renau
# Febrer 2023
# 
# Programa que rep almenys un més  o varis mesos. Per cada més indica quants dies té el més.
#
#   a) el programa valida que el número d'arguments rebuts és vàlid.
#   b) el programa valida per cada més que el seu valor és del 1 al 12. 
#      Si no ho és es mostra un missatge d'error per stderr però es continua processant la resta de mesos.
#   c) per cada més s'indica quants dies té.
# ----------------------------------------------------------------------------------------

# validacio arg
ERR_ARG=1
if [ $# -lt 1 ]; then
	echo "ERROR num arg no valid"
	echo "Usage $0"
	exit $ERR_ARG
fi

# valor messos
for mes in $*
do
	if ! [ $mes -ge 0 -a $mes -le 12 ]; then
		echo "ERROR, mes $mes no valid en [0-12]" >> error.log
	else
		case $mes in 
			[469] | "11" )
				dies=30
				;;
			"2" )
				dies=29
				;;
			* )
				dies=31

		esac
		echo "el mes $mes te $dies dies"
	fi
done

exit 0 








