#! /bin/bash
# Ton Renau

# Febrer 2023
#
# processar argumants
#  es corra fent bash 02-...sh aa bb  cc dd ee ff 
# ------------------------------------------------------------------
echo '$*: ' $*		# $* i $# llista els argumanets
echo '$@: ' $@ 				
echo '$#: ' $# 
echo '$0: ' $0 		# el 0 es el primer 
echo '$1: ' $1
echo '$9: ' $9
echo '$10: ' ${10}	# varialbeles llarges entre {}, el 11 esta mal 
echo '$11: ' $11
echo '$: ' $$ 		# pid 
echo "nomdeworld"

