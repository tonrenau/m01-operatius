#! /bin/bash
# Ton Renau
# febrer 2024 
# -----------------------------
if [ $# -eq 0 ] ;then
	echo "ERROR"
	echo " usage $0"
	exit 1
    
case $1 in 
	[aeiou])
		echo "la lletra es una vocal"
		;;
	[qwrtypsdfghjklñçzxcvbnm])
		echo "la lletra es consonant"
		;;
	*)
		echo "es un altre carater"
		;;
esac
exit 0