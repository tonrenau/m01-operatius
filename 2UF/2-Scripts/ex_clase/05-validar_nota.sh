#! /bin/bash 
# ton renau
# febrer 2024
# validar nota suspes aprovat
#  a) rep arg.
#  b) es del 0 a 10 
# ------------------------------------

# validacio arg 
ERR_ARG=1
ERR_NOTA=2
if [ $# -ne 1 ]
then
    echo "ERROR, num arg incorrect"
    echo "Usage, $0"
    exit $ERR_ARG
fi

# validacio nota [0,10]
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
    echo "ERROR, num $1 incorrect"
    echo "nota valors de [0,10]"
    echo "Usage: $0 nota"
    exit $ERR_NOTA
fi 
# prog 

nota=$1
if [ $nota -lt 5 ]
then 
	echo "Nota: $nota, suspes" 
else 
	echo "Nota: $nota, aprovat"
fi
exit 0
