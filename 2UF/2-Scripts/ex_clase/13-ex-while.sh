#! /bin/bash
# asix 
# 9 març 2023
# exemples while
#---------------------------

# 7) numera i mostra en mayusc l'entrada standard
cont=1
while  read -r line
do 
	echo "$cont:  $line" | tr 'a-z' 'A-Z'
	((cont++))
done
exit 0 
# 6) mostrar stdin linea a linea fins token fi 
read -r line 
while [ "$line" != "fi" ]
do
	echo $line
	read -r line 
done
exit 0 


# 5) numerar linea a linea
cont=1
while read -r line 
do
	echo $cont $line
	((cont ++))
done	
exit 0 
# 4) processar entrada standard linia a linia 
while read -r line  # mostrar linea a linea 
do
	echo $line
done
exit 0

# 3) iterar per llista d'arguments

while [ -n "$1" ]
do 
	echo "$1 $# $*"
	shift 		# desplaçament 
done
exit 0

# 2)
num=$1
while [ $num -ge 0 ]
do 
	echo $num
	((num--))
done
exit 0

# 1)
num=1 
while [ $num -le 10 ]
do 
	echo (-n) $num   # -n no salt de linea a la sortida
	((num++))
done
exit 0







