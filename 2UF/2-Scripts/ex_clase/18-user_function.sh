#! /bin/bash

function showUid(){

	uid=$1
	linea=$(grep "^[^:]*:[^:]*:" /etc/passwd)
	login=$(echo $linea |cut -d: -f1)
	gid=$(echo $linea |cut -d: -f4)
	gecos=$(echo $linea |cut -d: -f5)
	login=$(echo $linea |cut -d: -f6)
	shell=$(echo $linea |cut -d: -f7)

	echo "login: $login"
	echo "uid: $uid"
	echo "gid: $gid"
	echo "gecos: $gecos"
	echo "home: $home"
	echo "shell: $shell"
}
	
function suma() {
	suma=$(($1+$2))
	echo $suma	
	exit 0
}

function mult() {
	multi=$(($1*$2))
	echo $multi
	return 0
}

function show_llistaUids() {
	for uid in $*
	do 
		grep -q "^[^:]*:[^:]*:$uid:" /etc/passwd
		if [ $? -ne 0 ]; then
			echo "ERROR, UID $uid inexisten" > /dev/stderr
		else 
			echo $uid
			showUid $uid
			echo "-----------------------"
		fi 
	done
}

function informeShell () {
	llista_shells=$(cut -d: -f7 /etc/passwd | sort -u)
	for shell in $llista_shells
	do
		numlin=$grep -c ":$shell$" /etc/passwd
		if [ $numlin -ge 3 ]; then
			grep ":$shell$" /etc/passwd | sed -r 's/(.*)$/\t\1/'
 			echo "SHELL: $shell"
		fi 
	done
}
