#! /bin/bash
# Ton Renau
# 2 març 2023
# quants dies te el mes 
# ----------------------------------
# validar arguments
ERR_ARG=1
ERR_mes=2
if [ $# -ne 1 ] 
then
	echo "ERROR: num. arg. incorrecte"
	echo "USAGE: $0 mes"
	exit $ERR_ARG
fi 
# si el argument posat no est aentre el 1 i el 12 plegar
if ! [ $1 -ge 0 -a $1 -le 12 ]
then
	echo "ERROR: num. arg. incorrecte"
	echo "USAGE: valor between [1-12] mes"
	exit $ERR_mes 
fi 


# classificar els messos: 

case $1 in 
	[469]|"11")
		echo "el mes $1 ,te 30 dies"
		;;
	"2")
		echo "el mes 2, te 28 dies"
		;;
	*)
		echo "el mes $1, te 31 dies"

esac
exit 0

