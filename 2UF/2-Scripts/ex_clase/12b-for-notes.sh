#! /bin/bash
# Ton Renau
# Febrer 2023
# 
# Programa que rep almenys una nota o més  i per cada nota diu si és un suspès, aprovat, notable o excel·lent.
#	a) el programa valida que el número d'arguments és correcte.
# 	b) per cada nota valida que el valor és entre 0 i 10.
# 	   Si no és correcte es mostra un missatge per stderr però es continua iterant la resta de notes.
# 	c) per cada nota vàlida mostra quina qüalificació li correspòn.
#
# ------------------------------------------------------------------------------------

# validacio arguments
ERR_ATR=0
if [ $# -lt 1 ] ; then 
	echo "ERROR, too few arg"
	echo "USAGE: $0"
	exit $ERR_ATR
fi 

# iterar llista arg.
for note in $*
do
	if ! [ $note -ge 0 -a $note -le 10 ]; then
		echo "ERROR, nota $nota no valida [0-10]" >> error.log
	else
	# Qualificar la nota 
		if [ $note -lt 5 ] ; then 
			echo "Note: $note SUSPES"
		elif [ $note -lt 7 ]; then
			echo "Note: $note BE"
		elif [ $note -lt 9 ]; then 
			echo "Note: $note NOTABLE"
		else 
			echo "Note: $note EXELENT"
		fi
	fi
done


exit 0


