
opcions=""
arg=""
myfile=""
num=""
while [ -n "$1" ]
do
	case $1 in 
		-[abcd]) opcions="$opcions $1" ;;
		-f) myfile=$2
		    shift;;
		-n) num=$2
	   	    shift;;
		 *) arg="$arg $1";;
	esac 
 	shift 
done
echo "opcions: $opcions"
echo "arg: $arg"
echo "file: $myfile"
echo "num: $num" 
