#! /bin/bash
# 20 Març 2023 
# EDT ASIX 
# Exercis Scripts fitxa 2 
# ---------------------------------------------------------

# PROCESSAR ARGUMENTS

# 1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
for arg in $* 
do 
	paraula=$($# | wc -c) 
	if [ $paraula -ge  4 ]; then	 
		echo $paraula
	fi 
done

exit 0 






