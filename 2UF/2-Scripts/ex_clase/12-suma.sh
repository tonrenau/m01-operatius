#! /bin/bash
# Ton Renau 
# 7 Març 2023
# suma de arguments
# -------------------------
err_arg=1
if [ $# -eq 0 ]
then
	echo "ERROR: numero d'arguments incorrecte"
	echo "USAGE: minimum 1 argument."
	exit $Err_arg
fi 
suma=0
for nums in $*
do
	
	suma=$((suma +nums))

	echo $nums
done
echo $suma 
exit 0

