#! /bin/bash
# Ton Renau
# Febrer 2024
# dir els dies que te un mes 
# 	a) validar rep 1 arg
# 	b) validar mes [1-12]
# 	c) prog
#
# -------------------------------------------------

# a)
ERR_ARG=1
if [ $# -ne 1 ]; then
	echo "ERROR"
	echo "Usage $0"
	exit $ERR_ARG	
fi

# b)
mes=$1
ERR_MES=2
if ! [ $mes -ge 0 -a $mes -le 12 ]; then
	echo "ERROR, mes no valid"
	echo "Usage, $0, [0-12]"
	exit $ERR_MES
fi

# c)

case $mes in 
	[469] | "11" )
		dies=30
		;;
	"2" )
		dies=29
		;;
	* )
		dies=31

esac
echo "El mes $mes, te $dies dies"

exit 0

