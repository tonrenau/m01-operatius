#! /bin/bash
# Ton Renau

# Febrer 2023
#
# Exemple de primer programa
# Normes:
#   shebang (#!) indica qui ha d'interpretar el fitxer
#   caçalera: descripció, data, autor
# com guradaro al git: git add. git commit -m "materia" ; git push
# -----------------------------------------------

echo "hola mon"
nom="pere pou prat"
edat=25
echo $nom $edat
echo -e "nom: $nom\n edat: $edat\n"
echo -e 'nom: $nom\n edat: $edat\n'
uname -a 
uptime 
ps 
echo $SHELL 

# read data1 data2   --> interectuar amb el usuari ( no es fara servir, pq els que farem no interectuarem)
# echo -e "$data1 \n $data2"

exit 0  # a la hora de correr el programa, si s'inicia amb bash es tanca la sesio de bash creada, i si sinicia amb source o (.) es tancara la sessio.







