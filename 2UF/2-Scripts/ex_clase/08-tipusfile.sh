#! /bin/bash 
# Ton Renau
# Febrer 2023
# 
# indica si el prog es regular, dir, link o altre cosa 
# -----------------------------------

# valiadar arg
ERR_ARG=1 
if [ $# -ne 1 ]; then
	echo "ERROR, num arg incorrect"
	echo "Usage, $0"
	exit $ERR_ARG 
fi

# es directori 
tipus=$1
ERR_NOEXIST=1
if [ ! -e $tipus ]; then
	echo "$tipus no existeix"
	exit $ERR_NOEXIST
elif [ -d $tipus ];then
	echo "$tipus es directori"
elif [ -f $tipus ];then
	echo "$tipus es un regular file"
elif [ -h $tipus ]; then
	echo "$tipus es un link"
else 
	echo "Es una altre tipus"
fi
exit 0

