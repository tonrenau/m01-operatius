#! /bin/bash
# Ton Renau
# prog dir...
# rep un arg i es dir. i es llita interan per arg i dient quants son dir. file o altre cosa 
# quants !!!!
#--------------------------------------------------


# 1 validar arg
ERR_ARG=0

if [ $# -lt 1 ];then
    echo "ERROR: num arg no valid"
    echo "USAGE $0 dir..."
    exit $ERR_ARG
fi

# iterer per arg si es un dir.
for dir in $*
do 
    if ! [ -d $dir ];then
        echo "ERROR: $dir no es dir" >> error.log

    else
        # xixa
        llista=$(ls $dir)
        for elem in $llista
        do
            if [ -f $dir/$elem ]; then 
                echo " $elem: is regular file"
            elif [ -d $dir/$elem ]; then
                echo "$elem: is a directory"
            else
                echo "$elem: is another thing"
            fi
        done
    fi
done
exit 0