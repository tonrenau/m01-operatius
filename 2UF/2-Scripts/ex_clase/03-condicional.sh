#! /bin/bash 
# Ton Renau 
# Febrer 2023
#
# exemple if: indicar si es major d'edat
# $prog edat
# --------------------------------------------
# validar que existeix el arg.
if [ $# -ne 1 ]
then 
	echo "ERROR, num arg incorrect"
	echo "Usage: $0 edat"
	exit 1 
fi 

# prog

edat=$1
if [ $edat -lt 18 ]
then
	echo "edat $edat es menor"
elif [ $edat -lt 65 ] 
then 
	echo "edat $edat es legal"
else 
	echo "edat $edat es experimentada"
fi
exit 0



