#! /bin/bash 
#  EDT ASIX 2023 
# març 16 
# sinopsis: prog dir
# -------------------------------------------------------------
# sinopsis: prog dir
# validar que es rep un argument i que es un directori i llistar-ne el contingut.
# per llistar el contingut amb un ls ja ni ha prou 

# validar argument
err_arg=1
err_dir=2
if [ $# -eq 0 ] 
then 
	echo "ERROR: numero args no valid"
	echo "USAGE: $0 dir"
	exit $err_arg
fi

for dir in $*
do 
	if ! [ -d $dir ]; then
		echo "ERROR: $dir no es un dir"
	else	
		llista=$(ls $dir)
		echo "dir: $dir"
		for elem in $llista
		do 
			
			if [ -h "$dir/$elem" ]; then 
				echo -e "\t$elem es un link"
			elif [ -d "$dir/$elem" ]; then		
				echo -e "\t$elem es un directori"
			elif [ -f "$dir/$elem" ] ; then
				echo -e "\t$elem es un regular"
			else 
				echo -e  "\t$elem es una altre cosa"
			fi
		done
	fi
done
exit 0

