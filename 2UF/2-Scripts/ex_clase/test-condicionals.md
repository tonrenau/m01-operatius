Exercicis_de_test
1. file (element) not exists
# ! [ -e file ] ; echo $?
2. is not a regular file
# ! [ -f file ]; echo $!
3. element in the filesystem exists
# [ -e file ] ; echo $?
4. element is readable
# [ -r file ] ; echo $?  
5. num is not equal to 10
# [ $num -ne 10 ]; echo $?
6. num is less or equal to 100
# [ $numero -le 100 ] ; echo $? 
7. name is not pere, not marta not anna.
# [ $name != "pere" -a $name != "marta" -a $name != "anna" ]; echo $?
8. num is 10 or 15 or 20.
# [ $num -eq 10 -o $num -eq 15 -o $num -eq 20 ] ; echo $? 
9. num ins in the ranges [0,18], [65,120]
# [ $num -ge 0 -a $num -le 18 -o $num -ge 65 -a $num -le 120 ]
10. Num is not in the range [25,50]
# ! [ $num -ge 25 -a $num -le 50 ]
11. error if argos not: arg1 arg2 arg3
# [ $# -ne 3]
12. error if argos not: arg1...
# [ $# -eq 0 ] 
13. error if argos not: arg1 arg2...
# [ $# -lt 2 ]
14. var name has no value, is empty
# [ -z "$name" ]    
15. var num is not null
# [ -n "$num" ]     

# -n --> variable te contingut 
# -z --> variable es buida
# en -z i -n; ficar sempre la variable entre comes 



[ -eq --> = ; -ne --> != ; -gt --> > ; -lt --> <  ; -ge --> =>  ; ; -le --> =<]
[ -a --> and ; 	-o --> or ; ! --> not ]
