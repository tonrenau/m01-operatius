#! /bin/bash 
# Ton Renau
# maig 2023 
# 
# exercicis fitxa 2
# -------------------------------------------------- 








# 8 -----------------------------------

num_arg=1
if [ $# -eq 0 ]; then 
	echo "ERROR"
	echo "USAGE..:"
	exit $num_arg
fi

com=0 
err=0
for arg in $*
do 
	if ![ -f $arg ]; then 
		
	else 
		lines=$(cat $arg | wc -l)
		gzip $arg 
		if [ $? -eq 0 ]; then 
			((com++))
			echo "el fitxer $arg s'ha compimit" 
		else 
			echo "No s'ha pogut comprimir el fitxer $arg"
			((err++))
		echo "$arg te $lineas lineas compreses"
		fi 
	fi	
done 
echo "com: $com; err: $err" 
if [ $err -ne 0 ]; then 
	exit $err 
fi 
exit 0 


# 7 -------------------------------------
err_num_arg=1
if [ $# -ne 5 ];then
	echo "1"
	echo "ERROR, num arg incorrecte"
	echo "USAGE, $0 arguments"
	exit $err_num_arg
fi 

tipus=$1
shift 
count_arg=0
count_file=0
for arg in $*
do
	((count_arg++))
	if [ $tipus $arg ];then 
		((count_file++))
	fi
done	
if [ $count_arg -eq $count_file ];then
	echo "0 OK"
	echo "tots els fitxers son igulas, son tipus $1"
else
	echo "2"
	echo "ERROR, elements erronis"
	echo "USAGE, $0 arguments"
fi
exit 0









# exercici de validar si un user existeix o no  
oks=0
errors=0
for arg in $*
do 	
	grep -q $arg /etc/passwd 
	if [ $? -eq 0 ]; then 
		echo "User $arg ja existeix"
		((oks++))	
	else
		echo "user $arg no existeix" >> error.log 
		((errors++))
	fi
done
echo "$# $oks $errors"
if [ $errors -ne 0 ];then 
	exit $errors
fi
exit 0
