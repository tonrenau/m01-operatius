#! /bin/bash
# Ton Renau
# febrer 2024 
# -----------------------------
if [ $# -eq 0 ] ;then
	echo "ERROR"
	echo " usage $0"
	exit 1
fi

case $1 in 
	"pere"|"pau"|"joan")
		echo "Masculi"
		;;
	"marta"|"anna"|"julia")
		echo "Femeni"
		;;
	*)
		echo "no binari"
		;;
esac
exit 0