#! /bin/bash
# Ton Renau
# prog dir
# rep un arg i es dir. i es llita interan per arg i dient si es dir. file o altre cosa
#--------------------------------------------------

# 1 validar arg
ERR_ARG=0
ERR_NODIR=2
if [ $# -ne 1 ];then
    echo "ERROR: num arg no valid"
    echo "USAGE $0 dir"
    exit $ERR_ARG
fi

# validar que es un dir
dir=$1
if ! [ -d $dir ];then
    echo "ERROR: $dir no es dir"
    echo "USAGE: $0 dir"
    exit $ERR_NODIR
fi

# xixa

llista=$(ls $dir)
for elem in $llista
do
    if [ -f $dir/$elem ]; then 
        echo " $elem: is regular file"
    elif [ -d $dir/$elem ]; then
        echo "$elem: is a directory"
    else
        echo "$elem: is another thing"
    fi
done
exit 0