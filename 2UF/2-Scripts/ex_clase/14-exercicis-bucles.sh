#! bin/bash 
# 13 març
# -------------------------------------------

# 10. Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
num=$1
count=1
while read -r line 
do
	if [ $num -ge $count ]; then
		echo $count: $line
       		((count++))
	fi
done
exit 0

# 9. Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.

while read -r line 
do 
	validar=$(grep -wc "$line" /etc/passwd)
	if [ $validar -eq 1 ]
	then
		echo "$line, si existeix"
	else
		echo "$line, no pertany" >> error.log
	fi
done
exit 0


# 8. Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.

for nomUsuari in $*
do 
 	validar=$(grep -wc "$nomUsuari" /etc/passwd)	
	if [ $validar -eq ge]
	then
		echo "$nomUsuari, si existeix"
	else
		echo "$nomUsuari, no pertany" 2> error.log
	fi
done
exit 0 



# 7. Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.
caracter=60
while read -r line 
do	
	linea=$(echo "$line" |wc -c)	
	if [ $linea -ge $caracter ]
	then
		echo "$line" 
	fi
done	
exit 0 	

# 6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra quants dies eren laborables i quants festius. Si l’argument no és un dia de la setmana genera un error per stderr. 
# Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
case $1 in 
	"dilluns"|"dimart"|"dimecres"|"dijous"|"divendres")
		echo "$1, es un dia laborable"
		;;
	"dissabte"|"diumenge")
		echo "$1, dia de cap de semana"
		;;
	*)
		echo "No es un arg. valid"
esac
exit 0

# 5. Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
while read -r line
do 
	echo "$line " | cut -c1-50 
done
exit 0 
# 4. Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants  dies té el més.
case $1 in 
	[469]|"11") 		
		echo "el mes $1 ,te 30 dies" 		
		;; 	
	"2") 		
		echo "el mes 2, te 28 dies" 		
		;; 	
	*) 		
		echo "el mes $1, te 31 dies" 
esac 
exit 0

# 3. Fer un comptador des de zero fins al valor indicat per l’argument rebut.
count=0
NUM=$1
while [ $count -le $NUM ]
do 
	echo "$count"
	((count++))
done
exit 0

# 2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
count=1 
for arg in $*
do 
	echo "count: $arg"
	((count++))
done

exit 0 

# 1. Mostrar l’entrada estàndard numerant línia a línia
count=1 
while read -r line 
do
       echo "$count: $line"
       ((count++))

done
exit 0 

