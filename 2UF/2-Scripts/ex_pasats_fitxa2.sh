#! /bin/bash 

# --------------------------------------------------


err_num_arg=1
if [ $# -ne 5 ];then
	echo "1"
	echo "ERROR, num arg incorrecte"
	echo "USAGE, $0 arguments"
	exit $err_num_arg
fi 

tipus=$1
shift 
count_arg=0
count_file=0
for arg in $*
do
	((count_arg++))
	if [ $tipus $arg ];then 
		((count_file++))
	fi
done	
if [ $count_arg -eq $count_file ];then
	echo "0 OK"
	echo "tots els fitxers son igulas, son tipus $tipus"
else
	echo "2"
	echo "ERROR, elements erronis"
	echo "USAGE, $0 arguments"
fi
exit 0
