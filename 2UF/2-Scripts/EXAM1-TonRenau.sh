# /bin/bash 
# Ton Renau
# EDT 1r ASIX 
# Març 2024
#
#  a) verificar dir desti existeix sino error
#  b) validar files executables binaris, sino existeixen son errors recuperables, msg err per /dev/null
#  c) file ok > mostra el nom
#  d) contador err per al final del prog 
#  e)codis error: 1 = arg incorect, 2 = dir mo exist, 3 = si no hi ha agut algun fit exec bin, 0 ok 
#
# -----------------------------------------------------------------------------------------------

# validar arguments
ERR_NUM_ARG=1
if [ $# -le 1 ];then
	echo "ERROR: num arg incorrect"
	echo "USAGE: $0 prog"
	exit $ERR_NUM_ARG
fi
# validar dirDesti 
ERR_DIRDESTI=2
dir=$1
shift
if [ ! -e $dir ];then
	echo "ERROR: el directori desti $dir no existeix"
	echo "Synopsis: prog.sh dirDesti file[...]"
	exit $ERR_DIRDESTI
fi

# validar si els fitxers son executables binaris
fit_no_bin=0
for arg in $*
do
	if [ -e $arg ];then
		file -b $arg | cut -c1-3 | grep -q "ELF"
			if [ $? -eq 0 ];then	
				cp $arg $dir
				echo $arg
			else 
				echo "El fitxer $arg no es excutable-binari" >> /dev/null
				((fit_no_bin++))
			fi
	else
	       echo "ERR, $arg no existeix"	
	fi
done 
echo "Total fitxers no executables-binaris: $fit_no_bin"

# sortida d'error
if [ $fit_no_bin -eq 0 ];then
	exit 0
else
	exit 3
fi









