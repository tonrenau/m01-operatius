#! /bin/bash
# Ton Renau
# ex 9 - fitxa 2
# 
# Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg... 
# Escriure el codi que desa en les variables: 
# ​opcions​, ​cognom​, ​edat i ​arguments els valors corresponents. 
# No cal validar ni mostrar res! 
# 
# Per exemple si es crida: ​$ prog.sh -e 18 -r -c puig -j wheel postgres ldap 
#
# retona:  opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap» 
#
# ------------------------------------------------

