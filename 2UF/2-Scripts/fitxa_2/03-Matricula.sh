#! /bin/bash
# Ton Renau
# ex 3 - fitxa 2
# 
# Processar arguments que són matricules:  
#    a) Llistar les vàlides, del tipus: 9999-AAA.  
#    b) stdout les que són vàlides, per stderr les no vàlides. 
#       Retorna de status el número d’errors (de no vàlides).
# ---------------------------------------------------------------
err=0
for mat in $*
do 
    echo $mat | grep -qE "^[0-9]{4}-[A-Z]{3}$"
    if [ $? -eq 0 ];then 
        echo "$mat  OK"
    else :
        echo "ERROR, $mat nao valida" >> /dev/null
        ((err++))
    fi
done
exit $err


 