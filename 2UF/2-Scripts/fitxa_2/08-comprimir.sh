#! /bin/bash
# Ton Renau
# ex 8 - fitxa 2
# 
# Programa: ​prog file... 
#   a)  Validar existeix almenys un file. 
#       Per a cada file comprimir-lo. 
#       Generar per stdout el nom del file comprimit si s’ha comprimit correctament, 
#       o un missatge d’error per stderror si no s’ha pogut comprimir. 
#       En finalitzar es mostra per stdout quants files ha comprimit.  
#       
#       Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir. 
#   
#   b)  Ampliar amb el cas: ​prog -h|--help​
# -------------------------------------------------------------


ERR_NUM_ARG=1
# validar arg
if [ $# -eq 0 ]; then
    echo "ERROR!: num arg incorrect"
    echo "USAGE $0 prog"
    exit $ERR_NUM_ARG
fi

# xixa
ERR_COM=2
com_fil=0
err=0
for file in $*
do
    if  [ -f $file ];then 
        gzip $file
        if [ $? -eq 0 ];then
            echo "Nou nom: $file"
            ((com_fil++))
        else 
            echo "ERROR: No sa pogut comprimr" >> /dev/null
            ((err++))
        fi
    else 
        echo "ERROR: No es un file" >> /dev/null
        ((err++))
    fi
done

echo "File comprimtis amb exit: $com_fil"

if [ $err -gt 0 ]; then 
    exit $ERR_COM
else
    exit 0
fi
