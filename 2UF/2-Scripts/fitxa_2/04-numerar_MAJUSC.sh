#! /bin/bash
# Ton Renau
# ex 4 - fitxa 2
# 
# Processar stdin cmostrant per stdout les línies numerades i en majúscules
# ---------------------------------------------------


num_lin=1
while read -r line
do 
    echo $num_lin: $line | tr [a-z] [A-Z]
    ((num_lin++))
done
exit 0 