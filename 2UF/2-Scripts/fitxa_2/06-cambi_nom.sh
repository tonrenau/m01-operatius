#! /bin/bash
# Ton Renau
# ex 6 - fitxa 2
# 
# Processar per stdin línies d’entrada tipus “Tom Snyder” i mostrar 
# per stdout la línia en format → T. Snyder
# ------------------------------------------------

while read -r nom
do 
    echo $nom | grep -q "^[A-Z][a-z]* [A-Z][a-z]*"
    if [ $? -eq 0 ]; then
        echo "$nom" | sed -r "s/^([A-Z])[a-z]* ([A-Z][a-z]*)/\1. \2/"
    else 
        echo "ERROR, $nom nom no correcte"
    fi 
done 
exit 0