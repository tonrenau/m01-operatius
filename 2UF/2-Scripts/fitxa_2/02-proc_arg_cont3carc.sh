#! /bin/bash
# Ton Renau
# ex 2 - fitxa 2
# 
# Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
# ----------------------------------------------------------

count=0
for arg in $*
do
    echo $arg| grep -qE "^.{3,}"
    if [ $? -eq 0 ];then
        ((count++))
    fi
done
echo $count
exit 0
