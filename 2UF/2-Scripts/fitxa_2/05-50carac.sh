#! /bin/bash
# Ton Renau
# ex 5 - fitxa 2
# 
# Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
# ------------------------------------------------------

while read -r line 
do 
    echo $line | grep -E "^.{1,50}$"

done
exit 0
