#! /bin/bash
# Ton Renau
# ex 7 - fitxa 2
# 
# Programa: ​prog -f|-d arg1 arg2 arg3 arg4 
#   a)  Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a                                 
#       dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que                                     
#       tots quatre són directoris. 
#       Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.  
#       
#       Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.  
#       
#       b) Ampliar amb el cas: ​prog -h|--help​
# ------------------------------------------------------------------------

ERR_NUM_ARG=1
# validacio arg.
if [ $# -ne 5 ]; then 
    echo "ERROR, num arg incorrect"
    echo "USAGE: $0 args"
    exit $ERR_ARG
fi
# xixa
ERR_ARG=2
tipus=$1
shift 
for arg in $* 
do 
    if [ $tipus $arg ]; then 
        echo "$arg es valid"
    else 
        exit $ERR_ARG
    fi 
done 

exit 0 




