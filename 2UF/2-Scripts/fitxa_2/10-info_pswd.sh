#! /bin/bash
# Ton Renau
# ex 10 - fitxa 2
#   
# Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups
# , en format: gname: GNAME, gid: GID, users: USERS 
# 
# --------------------------------------------

while read -r group 
do 
    line=$(grep "^[^:]*:[^:]*:$group:" /etc/group)
    
    if [ $? -eq 0 ];then 
        echo "GNAME: $(echo $line| cut -d: -f1)"
        echo "GID: $(echo $line| cut -d: -f3)"
        echo "USERS: $(echo $line| cut -d: -f4)"
    else 
        echo "This group,$group, dosen't exist"
    fi
done
exit 0
