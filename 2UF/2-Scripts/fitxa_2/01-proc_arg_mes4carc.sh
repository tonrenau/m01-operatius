#! /bin/bash
# Ton Renau
# ex 1 - fitxa 2
# 
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters. 
# --------------------------------------------------

for arg in $*
do 
    carac=$(echo $arg | wc -c)

    if [ $carac -ge 4 ];then
        echo $arg
    fi
done
exit 0



