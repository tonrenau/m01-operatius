#! /bin/bash
# Ton Renau
# Ex 1 
# Mostrar l’entrada estàndard numerant línia a línia
# -------------------------------

count=1 
while read -r line 
do
    echo $count $line
    ((count++))
done

exit 0