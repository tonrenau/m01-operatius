#! /bin/bash
# Ton Renau
# Ex 6
# Fer un programa que rep com a arguments noms de dies de la setmana i mostra 
# quants dies eren laborables i quants festius. Si l’argument no és un dia de la 
# setmana genera un error per stderr. 
# 
# Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
# 
# ----------------------------------------------------------------------

dies_lab=0
dies_fest=0 
for dies in $*
do 
    case $dies in
        "dilluns" | "dimarts" | "dimecres" | "dijous" | "divendres" )
        ((dies_lab++))
        ;;
        "disabte" | "diumenge" )
        ((dies_fest++))
        ;;
        * )
        echo "--ERROR: No es un dia de la semana--" >> /dev/null
    esac
done 
echo "Hi han un total de $dies_lab dies laborables"
echo "Hi han un total de $dies_fest dies festius"
exit 0