#! /bin/bash
# Ton Renau
# ex 9 
# 
# Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el 
# sistema (en el fitxer /etc/passwd) mostra el nom per stdout. 
# Si no existeix el mostra per stderr.
# 
# --------------------------------------------------------

while read -r user
do 
    grep -wq ^$user: /etc/passwd
    if [ $? -eq 0 ]; then
        echo "El usuari, $user, es valid en el sistema."
    else
        echo "El usuari, $user, no es valid en el sistema." >> /dev/null
    fi 
done 
exit 0 