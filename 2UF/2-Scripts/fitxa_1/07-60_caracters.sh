#! /bin/bash
# Ton Renau
# Ex 7
# Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la 
# mostra, si no no.
# 
# -----------------------------------------------------

max_car=60 
while read -r line 
do 
    caracters=$(echo $line | wc -c) 
    if [ $caracters -ge $max_car ]; then
        echo $line
    fi 
done 
exit 0
