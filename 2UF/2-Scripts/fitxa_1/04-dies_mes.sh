#! /bin/bash
# Ton Renau
# Ex 4
# Fer un programa que rep com a arguments números de més (un o més) i indica per 
# a cada mes rebut quants dies té el més. 
# 
# ----------------------------------------------------------------------

ERR_ARG=1 

# Validacio arg 
if [ $# -eq 0 ]; then 
    echo "ERROR: num arg not correct"
    echo "USAGE: $0"
    exit $ERR_ARG
fi

# Valors del mes 

for mes in $*
do  

    if [ $mes -lt 0 -o $mes -gt 12 ]; then
        echo "ERROR: $mes no valid, use valors [0-12]" >> /dev/null 

    else
        case $mes in 
            "2" )
            dies=29
            ;;
            [469] | "11" )
            dies=30
            ;;
            * )
            dies=31
        esac
     echo "El mes, $mes, te $dies dies"
     fi 
done 
exit 0
