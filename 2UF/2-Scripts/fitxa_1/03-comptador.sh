#! /bin/bash
# Ton Renau
# Ex 3
# Fer un comptador des de zero fins al valor indicat per l’argument rebut.
# ---------------------------------------

max=$1
num=0 
while [ $num -le $max ]
do 
    echo "$num"
    ((num++))
done
exit 0