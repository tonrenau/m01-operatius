#! /bin/bash
# Ton Renau
# Ex 8
# 
# Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema 
# (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per 
# stderr.
# 
# ---------------------------------------------------------------

 
for user in $* 
do 
    grep -qw ^$user: /etc/passwd

    if [ $? -eq 0 ]; then
        echo "El usuari, $user, si que es valid en el sistema"
    else
        echo "ERROR: El usuari, $user, no es valid en el sistema" >> /dev/null
    fi
done
exit 0