#! /bin/bash 
# Ton Renau
# ex 2
# Mostar els arguments rebuts línia a línia, tot numerànt-los.
# -----------------------------------------------

count=1
for arg in $*
do 
    echo "$count.- $arg"
    ((count++))
done 
exit 0