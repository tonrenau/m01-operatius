#! /bin/bash
# Ton Renau
# ex 10 
# 
# Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. 
# El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
# 
# ---------------------------------------------------------
count=1
max=$1
FI=0
while read -r line
do 
    echo "$count: $line"
    ((count++))
    if [ $max -lt $count ]; then 
        exit $FI
    fi
done
exit 0  
