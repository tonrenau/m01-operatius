#! /bin/bash 

err=0
while read -r service
do 
	systemctl is-active $service &> /dev/null
	if [ $? -eq 0 ]; then 
		echo "the service $service is active "
	else 
		(( err++))
	fi	
done
exit $err





tots_gids=$(cut -d: -f4 /etc/passwd)
llista_gids=$(cut -d: -f4 /etc/passwd | sort -n | uniq)

for gid in $llista_gids
do 
	num=$(echo $tots_gids | grep -wc  "^$gid$")
	echo "$gid: $num"
done

exit 0 
