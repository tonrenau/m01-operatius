# /bin/bash 
# Ton Renau

# 1 --> itera per una llista

FRUITES="poma, pera, platan"

for fruit in $FRUITES
do
    echo $fruit
done
exit 0

# Funcio on que si rep un arg el procesa sino proscesa per stdin

function check(){
    fileIn="/dev/stdin"
    if [ $# -eq 1 ]; then 
        fileIn=$1
    fi

    while read -r line
    do
        echo $line
    done < $fileIn 
}

# funcio on el user entra el gname i mostra el login 

function mida(gname) {
    #buscar el gid
    gid=grep "^gname:" /etc/passwd | cut -d: -f3
    #obtenir el llistat de logins
    list_login=grep "^[*:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1

    # iterar per cada login 
    for log in $list_login
    do 
        echo $log
    done 
}
