# Targets d’arrencada -------------------------


## 1. En tots els casos verificar el target actual.  Llisteu els processos i els serveis.
systemctl get-default 

systemctl list-dependencies

## 2. Configurar el sistema establint default.target a mode multi-user.target.
root@debian2:~# systemctl set-default multi-user.target 

Es fa un remove del link anterior i es crea el symbolic link de /etc/systemd/system/default.target -->  /lib/systemd/system/multi-user.target

root@debian2:~# systemctl get-default
multi-user.target

## 3. Manualment canviar a graphical.target amb isolate.
systemctl isolate graphical.target

## 4. Reiniciar el sistema i al grub etablir l’opció de iniciar en mode emergency.target.
e --> edit
En la linea de linux ficar al final 
systemd.unit=emergency.target

## 5. Reiniciar i indicar al grub l’opció del kernel per iniciar a rescue.target.
e --> edit
Al final de la linea linux ficar 
 - systemd.unit=rescue.target 
 - 1

## 6. Canviar de target amb l’ordre isolate activant multi-user.target.
systemctl isolate multi-user.target

## 7. Restablit per defecte graphical.target i reiniciar el sistema.
systemctl set-default graphical.target

## 8. Amb isolate indicar que es vol accedir al target poweroff.target.
systemctl isolate poweroff.target

## 9. Iniciar el sistema en mode emergency.target. Llistar els processos, l’arbre de processos, els units i les dependencies. Cal indicar el password de root? Hi ha multiples sessions de consola?
e --> edit
En la linea de linux ficar al final 
systemd.unit=emergency.target

ps ax --> on hi han molts pocs prcessos
systemctl list-unit-files --type service

systemctl list-dependencies emergency.target

## 10. Iniciar el sistema en mode init=/bin/bash. Llistar els processos, l’arbre de processos, els units i les dependencies. Cal indicar el password de root? Hi ha multiples sessions de consola?
En la linea de linux de (e(edit)) esteblim al final de la linea init=/bin/bash.

ps -ax 

systemctl list-unit-files

No deixa fer el systemctl list-dependencies ja que no a iniciat amb ningun target 

ETS ROOT SENSE CAP PASWD
 
# Gestió bàsica de serveis-------------------------------------
## 11. Instal-leu els serveis httpd, gpm, xinetd i ssh.
apt-get install httdirfs gpm xinetd ssh

## 12. Activeu els servei ssh i configureu-los disabled. Verifiqueu que estan actius. Reiniciar el sistema i observar que no estan Iniciar el sistema.
systemctl status ssh.service --> ACTIVE 
systemctl disable ssh.service
reboot
systemctl status ssh.service --> INACTIVE


## 13. Amb els serveis inactius condfigurar-los (httpd i sshd) com a enabled. Verificar que estan enables però inactius. Reiniciar el sistema i observar que estan actius i enabled.
systemctl status ssh.service --> INACTIVE
systemctl enable ssh
reboot 
systemctl status ssh.service --> ACTIVE 

## 14. Fer mask del servei httpd i observar que no es pot fer enable. Fer unmask. Serveis i Units
systemctl mask ssh 
systemctl enable ssh --> No deixa (mask u bloqueja)
systemctl unmask ssh 
systemctl enable ssh --> ara si

## 15. Identificar el fitxer .service del servei ssh. Identificar l’executable.
ls /lib/systemd/system/ssh.service

## 16. Configurar manualment amb symlinks el enable del servei xinetd i verificar-ho.
enable: 
craete symlink:
 /etc/systemd/system/sshd.service -> /lib/systemd/system/ssh.service
 /etc/systemd/system/multi-user.target.wants/ssh.service -> /lib/systemd/system/ssh.service


PRACTIC:
systemctl status ssh --> INACTIVE; DISABLE 
ln -s /lib/systemd/system/ssh.service /etc/systemd/system/sshd.service

ln -s /lib/systemd/system/ssh.service /etc/systemd/system/multi-user.target.wants/ssh.service

reboot

systemctl status ssh --> ACTIVE; ENABLE 


## 17. Eliminar el enable del servei xinetd manualment amb el symlink. Verificar-ho.
disable:
removed /etc/systemd/system/multi-user.target.wants/ssh.service
removed /etc/systemd/system/sshd.service

systemctl status ssh.service --> ACTIVE; enable
rm /etc/systemd/system/multi-user.target.wants/ssh.service
rm /etc/systemd/system/sshd.service

systemctl stop ssh.service 
reboot

systemctl status ssh.service --> INACTIVE; disabled


# Arquitectura de systemd ------------------------------------
## 18. Instal·la el paquet ssh. Llista els seus components.
/lib/systemd/system/ssh.service

## 19. Identifica el fitxer de servei de ssh i llista’l. Identifica l’executable del servei i a quin target s’instal·la.
systemctl list-dependencies

i obserbem que s'instala amb el multi-user.target

## 20. Llista el directori on hi ha els fitxers de les units de servei de multi-user.target. Identifica el fitxer corresponent al servei ssh.
ls /etc/systemd/system /multi-user.target.wants/ssh.service

## 21. Fes enable del servei xinetd. S’ha creat un link. Llista el directori origen i el directori destí del link. On s’ha creat el link? Perquè?

## 22. Llista les dependències de multi-user.target: totes; només els targets; només els serveis.
systemctl list-dependencies | grep .target

systemctl list-dependencies | grep .service

## 23. Llista les dependències de xinetd.service.
systemctl list-dependencies | grep ssh.service

## 24. Fes l’ordre systemctl show del servei xinetd i identifica els elements: After i WantedBy.
systemctl show ssh

After=.mount basic.target ....
WantedBy=multi-user.target
## 25. Es pot iniciar sessió en el sysinit.target? Explica el perquè documentant-ho.
No es pot iniciar
Ja que es un target que representa la fase d'inicialitzacio del sistema 


# Arrencada de targets diferents --------------------------
## 26. Mostra les dependències de recue.target: totes; targets; serveis.
systemctl list-dependencies | grep rescue.target
## 27. Mostra les dependències de emergency.target: totes; targets; serveis.
systemctl list-dependencies | grep rescue.target
## 28. Estableix com a target per defecte reboot.target i reinicia la màquina.
systemctl set-default reboot.target

reboot

## 29. Punyeta! Arregla-h
Al grub:
e(edit)
linux... systemd.unit=graphical.target