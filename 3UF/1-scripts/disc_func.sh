#! /bin/bash 
# EDT març 2024
# Ton Renau 
# 
# funcions de disc
# ----------------------------------------------

function fsize(){
    login=$1
    line=$(grep "^$login:" /etc/passwd)
    if [ -z "$line"  ]; then 
        echo "ERROR..."
        return 1
    fi
    home=$(echo $line | cut -d: -f6 )
    echo $home
    du -sh $home
}

# rep arg qe son logins i calcula el fsize del home
function loginargs(){
    if [ $# -eq 0 ]; then 
        echo "ERR..."
        return 1
    fi
    for login in $*
    do 
        fsize $login
    done

}

# rep un arg=fitxer i cada line del fit. te un login
function loginfile() {
    if [ ! -e $1 ]; then 
        echo "No exist.."
        return 1
    fi
    file=$1
    while read -r login
    do 
        fsize $login
    done < $file
}

# rep log per stdin per cada un calcula el size
function loginstdin() {
    while read -r login
    do 
        fsize $login
    done
}

# processa el fit rebut per 
function loginboth(){
    file="/dev/stdin"
    if [ $# -eq 1 ];then 
        file=$1
    fi

    while read -r login
    do
        fsize $login
    done < $file
}