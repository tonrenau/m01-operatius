#! /bin/bash 
# EDT març 2024
# Ton Renau 
# 
# funcions
# ----------------------------------------------

# SEMPRE S'HA DE CARRGAR LES FUNCIONS --> . prog.sh

function suma (){
    echo $(($1 + $2))
    return 0
}

# En scrpiting les funcions no retornen, sempre es retunra un return 

function showUserByLogin(){
    # validar n arg

    # validar user existeix
    login=$1 
    line=$(grep "^$login:" /etc/passwd)
    if [ -z "$line" ];then
        echo "ERROR.."
        return 1
    fi
    uid=$(echo $line | cut -d: -f3)
    gid=$(echo $line | cut -d: -f4 )
    shell=$(echo $line | cut -d: -f7)
    echo "login: $login"
    echo "uid: $uid"
    echo "gid: $gid"
    echo "shell: $shell"
    return 0
}


function showUserInGroupByGid(){
    # validar args

    # validar existeix group, mostrar gname(gid)
    grup=$1 
    gname=$(grep  "^[^:]*:[^:]*:$grup:" /etc/group | cut -d: -f1)
    if [ -z $gname ]; then 
        echo "ERROR..."
        return 1
    fi
    echo "Grup: $gname($grup)"
    # login, uid, shell <-- gid 
    grep "^[^:]*:[^:]*:[^:]*:$grup:" /etc/passwd | cut -d: -f1,3,7

    return 0
}


function showAllShells(){
    MIN=$1
    # obtenir shells
    list_shell=$(cut -d: -f7 /etc/passwd | sort | uniq)
    # iterar per cada shell
    for shell in $list_shell
    do
        num_user=$(grep ":$shell$" /etc/passwd | cut -d: -f1 | wc -l)
        if [ $num_user -gt $MIN ]; then 
            echo "------ $shell: ($num_user) --------------------"
            # buscar el user de cada shell
            grep ":$shell$" /etc/passwd | cut -d: -f1 | sort | sed -r "s/^(.*)$/\t \1/"
        fi
    done
    return 0
}